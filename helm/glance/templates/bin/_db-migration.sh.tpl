#!/usr/bin/env bash

#@Todo. Add condition to generate db part only during install (if release.IsInstall)
#
#
echo "Database migration ..."

glance-manage db expand
glance-manage db migrate
glance-manage db contract
exit 0