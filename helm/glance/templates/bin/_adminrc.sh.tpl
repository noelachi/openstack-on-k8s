#!/usr/bin/env bash

export OS_USERNAME={{ .Values.keystone.adminUser }}
export OS_PASSWORD={{ .Values.keystone.adminPassword }}
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://{{ .Values.keystone.host }}:5000/v3
export OS_IDENTITY_API_VERSION=3