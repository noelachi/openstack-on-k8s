
#!/usr/bin/env bash

for i in {1..30}
 do
 sleep 3
 curl http://keystone:5000/v3

 if [ $? -eq 0 ]; then
   source /scripts/adminrc.sh
   openstack user create --domain default --password {{ .Values.authentication.password }} glance
   openstack role add --project service --user glance admin
   openstack service create --name glance --description "OpenStack Image" image
   openstack endpoint create --region {{ .Values.authentication.regionName }} image public http://{{ .Values.authentication.externalURL }}
   openstack endpoint create --region {{ .Values.authentication.regionName }} image internal http://{{ include "glance.fullname" . }}:9292
   openstack endpoint create --region {{ .Values.authentication.regionName }} image admin http://{{ include "glance.fullname" . }}:9292
   exit 0
 else
   echo $(date)" OpenStack API endpoint not available at this time."
 fi
 done
exit 1




