#!/usr/bin/env bash

#@Todo. Add condition to generate db part only during install (if release.IsInstall)
#
#
echo "Database initialization ..."
MARIADB_HOST={{ .Values.mariadb.host | quote }}
MARIADB_ROOT_PASSWORD={{ .Values.mariadb.rootPassword | quote}}
GLANCE_DB_PASSWORD={{ .Values.authentication.password | quote}}
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "CREATE DATABASE IF NOT EXISTS glance;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'localhost' IDENTIFIED BY '${GLANCE_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'%' IDENTIFIED BY '${GLANCE_DB_PASSWORD}' WITH GRANT OPTION;"

echo "Testing the Keystone database"
mysql -u glance --password="${GLANCE_DB_PASSWORD}" --host="${MARIADB_HOST}" --database="glance" -e 'show databases;'
if [[ $? != 0 ]]; then
echo "Keystone database test failed ..."
else
echo "Keystone database test completed ..."
fi


glance-manage db_sync
glance-manage db_load_metadefs
exit 0