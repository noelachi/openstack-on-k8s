[global]
mon_host = {{ .Values.ceph.monHosts }}

[client.glance]
keyring = /etc/ceph/ceph.client.glance.keyring
