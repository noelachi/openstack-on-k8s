#!/bin/sh
GALERA_ROOT_PASSWORD={{ .Values.galera.rootUser.password | quote}}
KEYSTONE_DB_PASSWORD={{ .Values.galera.dbPassword | quote}}
if [[ $(hostname) == *-0  ]]; then
  echo "First node"
  mysql -u root --password="${GALERA_ROOT_PASSWORD}"  -e "CREATE DATABASE keystone;"
  mysql -u root --password="${GALERA_ROOT_PASSWORD}"  -e "GRANT ALL PRIVILEGES ON *.* TO 'keystone'@'localhost' IDENTIFIED BY '${KEYSTONE_DB_PASSWORD}' WITH GRANT OPTION;"
  mysql -u root --password="${GALERA_ROOT_PASSWORD}"  -e "GRANT ALL PRIVILEGES ON *.* TO 'keystone'@'%' IDENTIFIED BY '${KEYSTONE_DB_PASSWORD}' WITH GRANT OPTION;"
else
  echo "No first node"
fi