[DEFAULT]
debug = False
transport_url = rabbit://{{ .Values.rabbitmq.auth.username }}:{{ .Values.rabbitmq.auth.password }}@{{ .Values.rabbitmq.fullnameOverride }}:5672//
log_file = /var/log/keystone/keystone.log

[oslo_middleware]
enable_proxy_headers_parsing = True

[database]
connection = mysql+pymysql://{{ .Values.global.dbUser }}:{{ .Values.global.dbPassword }}@{{ .Values.galera.fullnameOverride }}:3306/keystone
max_retries = -1

[token]
revoke_by_id = False
provider = fernet
expiration = 86400
allow_expired_window = 172800

[credential]

[fernet_tokens]
max_active_keys = 3

[cache]
backend = dogpile.cache.memcached
enabled = True
memcache_servers = {{ .Values.memcached.fullnameOverride }}:11211

[oslo_messaging_notifications]
transport_url = rabbit://{{ .Values.rabbitmq.auth.username }}:{{ .Values.rabbitmq.auth.password }}@{{ .Values.rabbitmq.fullnameOverride }}:5672//
driver = noop

[cors]
#allowed_origin = http://ORIGIN_HOST:3000