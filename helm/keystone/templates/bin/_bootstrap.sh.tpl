#!/usr/bin/env bash

#set -ex

keystone-manage  bootstrap --bootstrap-password {{ .Values.keystone.adminPassword }} \
--bootstrap-admin-url http://{{ .Values.keystone.service.host }}:5000/v3/ \
--bootstrap-internal-url http://{{ .Values.keystone.service.host }}:5000/v3/ \
--bootstrap-public-url http://{{ .Values.global.apiEndointName }}.{{ .Values.global.wildcardDnsName }}/v3/ \
--bootstrap-region-id {{ .Values.global.region }}

if [ $? -eq 0 ]; then
echo "Keystone bootstrap completed ..."
else
echo "Keystone bootstrap failed ..."
exit 1
fi
