#!/bin/bash

#set -e

#@Todo. Add condition to generate db part only during install (if release.IsInstall)
#
#
echo "Database initialization ..."
GALERA_HOST={{ .Values.galera.fullnameOverride | quote }}
GALERA_ROOT_PASSWORD={{ .Values.galera.rootUser.password | quote}}
KEYSTONE_DB_PASSWORD={{ .Values.global.dbPassword | quote}}
mysql -u root --password="${GALERA_ROOT_PASSWORD}" --host="${GALERA_HOST}" -e "CREATE DATABASE IF NOT EXISTS keystone;"
mysql -u root --password="${GALERA_ROOT_PASSWORD}" --host="${GALERA_HOST}" -e "GRANT ALL PRIVILEGES ON *.* TO 'keystone'@'localhost' IDENTIFIED BY '${KEYSTONE_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${GALERA_ROOT_PASSWORD}" --host="${GALERA_HOST}" -e "GRANT ALL PRIVILEGES ON *.* TO 'keystone'@'%' IDENTIFIED BY '${KEYSTONE_DB_PASSWORD}' WITH GRANT OPTION;"

echo "Testing the Keystone database"
mysql -u keystone --password="${KEYSTONE_DB_PASSWORD}" --host="${GALERA_HOST}" --database="keystone" -e 'show databases;'
if [[ $? != 0 ]]; then
echo "Keystone database test failed ..."
else
echo "Keystone database test completed ..."
fi

echo "DB Version before migration:"
keystone-manage --config-file=/etc/keystone/keystone.conf db_version

keystone-manage --config-file=/etc/keystone/keystone.conf db_sync --check
case $? in
    0)
        echo "No migration required. Database is up-2-date."
        ;;
    1)
        echo "Uhoh, Houston we have a problem."
        ;;
    2)
        echo "Database update available - starting migrations"
        # expand the database schema
        keystone-manage --config-file=/etc/keystone/keystone.conf db_sync --expand       
        #su -s /bin/sh -c "keystone-manage db_sync --expand" keystone
        if [[ $? != 0 ]]; then
        echo "Keystone database expand failed ..."
        else
        echo "Keystone database expand completed ..."
        fi
        ;&
    3)
        echo "Database expanded"
        # run migrate
        keystone-manage --config-file=/etc/keystone/keystone.conf db_sync --migrate
        #su -s /bin/sh -c "keystone-manage db_sync --migrate" keystone
        if [[ $? != 0 ]]; then
        echo "Keystone database migrate failed ..."
        else
        echo "Keystone database migrate completed ..."
        fi
        ;&
    4)
        echo "Database migrated"
        # run contraction
        keystone-manage --config-file=/etc/keystone/keystone.conf db_sync --contract
        if [[ $? != 0 ]]; then
        echo "Keystone database contract failed ..."
        else
        echo "Keystone database contract completed ..."
        fi
        ;;
    *)
        echo "Duno what state the database is in. grrrr"
        ;;
esac

# don't let the doctor break stuff (as usual not qualified enough and you allways need another opinion :P )
echo "Keystone doctor:"
keystone-manage --config-file=/etc/keystone/keystone.conf doctor
exit 0