
#!/usr/bin/env bash

for i in {1..30}
 do
 sleep 3
 curl http://keystone:5000/v3

 if [ $? -eq 0 ]; then
   source /scripts/adminrc.sh
   openstack project create --domain default --description "Service Project" service

   exit 0
 else
   echo $(date)" OpenStack API endpoint not available at this time."
 fi
 done
exit 1




