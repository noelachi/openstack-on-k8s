deprecated_options:
  DATABASE:
  - name: sql_connection
    replacement_group: database
    replacement_name: connection
  - name: idle_timeout
    replacement_group: database
    replacement_name: connection_recycle_time
  - name: sql_idle_timeout
    replacement_group: database
    replacement_name: connection_recycle_time
  - name: sql_max_pool_size
    replacement_group: database
    replacement_name: max_pool_size
  - name: sql_max_retries
    replacement_group: database
    replacement_name: max_retries
  - name: reconnect_interval
    replacement_group: database
    replacement_name: retry_interval
  - name: sqlalchemy_max_overflow
    replacement_group: database
    replacement_name: max_overflow
  - name: sqlalchemy_pool_timeout
    replacement_group: database
    replacement_name: pool_timeout
  DEFAULT:
  - name: rpc_conn_pool_size
    replacement_group: DEFAULT
    replacement_name: rpc_conn_pool_size
  - name: rpc_thread_pool_size
    replacement_group: DEFAULT
    replacement_name: executor_thread_pool_size
  - name: log_config
    replacement_group: DEFAULT
    replacement_name: log-config-append
  - name: logfile
    replacement_group: DEFAULT
    replacement_name: log-file
  - name: logdir
    replacement_group: DEFAULT
    replacement_name: log-dir
  - name: bind_host
    replacement_group: eventlet_server
    replacement_name: public_bind_host
  - name: public_bind_host
    replacement_group: eventlet_server
    replacement_name: public_bind_host
  - name: public_port
    replacement_group: eventlet_server
    replacement_name: public_port
  - name: bind_host
    replacement_group: eventlet_server
    replacement_name: admin_bind_host
  - name: admin_bind_host
    replacement_group: eventlet_server
    replacement_name: admin_bind_host
  - name: admin_port
    replacement_group: eventlet_server
    replacement_name: admin_port
  - name: policy_file
    replacement_group: oslo_policy
    replacement_name: policy_file
  - name: policy_default_rule
    replacement_group: oslo_policy
    replacement_name: policy_default_rule
  - name: policy_dirs
    replacement_group: oslo_policy
    replacement_name: policy_dirs
  - name: osapi_max_request_body_size
    replacement_group: oslo_middleware
    replacement_name: max_request_body_size
  - name: max_request_body_size
    replacement_group: oslo_middleware
    replacement_name: max_request_body_size
  - name: notification_driver
    replacement_group: oslo_messaging_notifications
    replacement_name: driver
  - name: notification_transport_url
    replacement_group: oslo_messaging_notifications
    replacement_name: transport_url
  - name: notification_topics
    replacement_group: oslo_messaging_notifications
    replacement_name: topics
  - name: amqp_auto_delete
    replacement_group: oslo_messaging_rabbit
    replacement_name: amqp_auto_delete
  - name: kombu_reconnect_delay
    replacement_group: oslo_messaging_rabbit
    replacement_name: kombu_reconnect_delay
  - name: rabbit_login_method
    replacement_group: oslo_messaging_rabbit
    replacement_name: rabbit_login_method
  - name: rabbit_retry_backoff
    replacement_group: oslo_messaging_rabbit
    replacement_name: rabbit_retry_backoff
  - name: rabbit_ha_queues
    replacement_group: oslo_messaging_rabbit
    replacement_name: rabbit_ha_queues
  - name: sqlite_synchronous
    replacement_group: database
    replacement_name: sqlite_synchronous
  - name: db_backend
    replacement_group: database
    replacement_name: backend
  - name: sql_connection
    replacement_group: database
    replacement_name: connection
  - name: sql_idle_timeout
    replacement_group: database
    replacement_name: connection_recycle_time
  - name: sql_max_pool_size
    replacement_group: database
    replacement_name: max_pool_size
  - name: sql_max_retries
    replacement_group: database
    replacement_name: max_retries
  - name: sql_retry_interval
    replacement_group: database
    replacement_name: retry_interval
  - name: sql_max_overflow
    replacement_group: database
    replacement_name: max_overflow
  - name: sql_connection_debug
    replacement_group: database
    replacement_name: connection_debug
  - name: sql_connection_trace
    replacement_group: database
    replacement_name: connection_trace
  amqp1:
  - name: container_name
    replacement_group: oslo_messaging_amqp
    replacement_name: container_name
  - name: idle_timeout
    replacement_group: oslo_messaging_amqp
    replacement_name: idle_timeout
  - name: trace
    replacement_group: oslo_messaging_amqp
    replacement_name: trace
  - name: ssl_ca_file
    replacement_group: oslo_messaging_amqp
    replacement_name: ssl_ca_file
  - name: ssl_cert_file
    replacement_group: oslo_messaging_amqp
    replacement_name: ssl_cert_file
  - name: ssl_key_file
    replacement_group: oslo_messaging_amqp
    replacement_name: ssl_key_file
  - name: ssl_key_password
    replacement_group: oslo_messaging_amqp
    replacement_name: ssl_key_password
  - name: sasl_mechanisms
    replacement_group: oslo_messaging_amqp
    replacement_name: sasl_mechanisms
  - name: sasl_config_dir
    replacement_group: oslo_messaging_amqp
    replacement_name: sasl_config_dir
  - name: sasl_config_name
    replacement_group: oslo_messaging_amqp
    replacement_name: sasl_config_name
  - name: server_request_prefix
    replacement_group: oslo_messaging_amqp
    replacement_name: server_request_prefix
  - name: broadcast_prefix
    replacement_group: oslo_messaging_amqp
    replacement_name: broadcast_prefix
  - name: group_request_prefix
    replacement_group: oslo_messaging_amqp
    replacement_name: group_request_prefix
  assignment:
  - name: caching
    replacement_group: resource
    replacement_name: caching
  - name: cache_time
    replacement_group: resource
    replacement_name: cache_time
  - name: list_limit
    replacement_group: resource
    replacement_name: list_limit
  database:
  - name: idle_timeout
    replacement_group: database
    replacement_name: connection_recycle_time
  oslo_messaging_rabbit:
  - name: rabbit_use_ssl
    replacement_group: oslo_messaging_rabbit
    replacement_name: ssl
  - name: kombu_ssl_version
    replacement_group: oslo_messaging_rabbit
    replacement_name: ssl_version
  - name: kombu_ssl_keyfile
    replacement_group: oslo_messaging_rabbit
    replacement_name: ssl_key_file
  - name: kombu_ssl_certfile
    replacement_group: oslo_messaging_rabbit
    replacement_name: ssl_cert_file
  - name: kombu_ssl_ca_certs
    replacement_group: oslo_messaging_rabbit
    replacement_name: ssl_ca_file
  - name: kombu_reconnect_timeout
    replacement_group: oslo_messaging_rabbit
    replacement_name: kombu_missing_consumer_retry_timeout
  profiler:
  - name: profiler_enabled
    replacement_group: profiler
    replacement_name: enabled
  rpc_notifier2:
  - name: topics
    replacement_group: oslo_messaging_notifications
    replacement_name: topics
  sql:
  - name: connection
    replacement_group: database
    replacement_name: connection
  - name: idle_timeout
    replacement_group: database
    replacement_name: connection_recycle_time
  token:
  - name: revocation_cache_time
    replacement_group: revoke
    replacement_name: cache_time
generator_options:
  config_dir: []
  config_file:
  - config-generator/keystone.conf
  config_source: []
  format_: yaml
  minimal: false
  namespace:
  - keystone
  - oslo.cache
  - oslo.log
  - oslo.messaging
  - oslo.policy
  - oslo.db
  - oslo.middleware
  - osprofiler
  output_file: keystone-schema.yaml
  summarize: false
  wrap_width: 79
options:
  DEFAULT:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: admin_token
      help: Using this feature is *NOT* recommended. Instead, use the `keystone-manage
        bootstrap` command. The value of this option is treated as a "shared secret"
        that can be used to bootstrap Keystone through the API. This "token" does
        not represent a user (it has no identity), and carries no explicit authorization
        (it effectively bypasses most authorization checks). If set to `None`, the
        value is ignored and the `admin_token` middleware is effectively disabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: admin_token
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: public_endpoint
      help: 'The base public endpoint URL for Keystone that is advertised to clients
        (NOTE: this does NOT affect how Keystone listens for connections). Defaults
        to the base host URL of the request. For example, if keystone receives a request
        to `http://server:5000/v3/users`, then this will option will be automatically
        treated as `http://server:5000`. You should only need to set option if either
        the value of the base URL contains a path that keystone does not automatically
        infer (`/prefix/v3`), or if the endpoint should be found on a different host.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: public_endpoint
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: uri value
    - advanced: false
      choices: []
      default: 5
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_project_tree_depth
      help: 'Maximum depth of the project hierarchy, excluding the project acting
        as a domain at the top of the hierarchy. WARNING: Setting it to a large value
        may adversely impact performance.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_project_tree_depth
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 64
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_param_size
      help: Limit the sizes of user & project ID/names.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_param_size
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 255
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_token_size
      help: Similar to `[DEFAULT] max_param_size`, but provides an exception for token
        values. With Fernet tokens, this can be set as low as 255.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_token_size
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: list_limit
      help: The maximum number of entities that will be returned in a collection.
        This global limit may be then overridden for a specific driver, by specifying
        a list_limit in the appropriate section (for example, `[assignment]`). No
        limit is set by default. In larger deployments, it is recommended that you
        set this to a reasonable number to prevent operations like listing all users
        and projects from placing an unnecessary load on the system.
      max: null
      metavar: null
      min: null
      mutable: false
      name: list_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: strict_password_check
      help: If set to true, strict password length checking is performed for password
        manipulation. If a password exceeds the maximum length, the operation will
        fail with an HTTP 403 Forbidden error. If set to false, passwords are automatically
        truncated to the maximum length.
      max: null
      metavar: null
      min: null
      mutable: false
      name: strict_password_check
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: insecure_debug
      help: If set to true, then the server will return information in HTTP responses
        that may allow an unauthenticated or authenticated user to get more information
        than normal, such as additional details about why authentication failed. This
        may be useful for debugging but is insecure.
      max: null
      metavar: null
      min: null
      mutable: false
      name: insecure_debug
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_publisher_id
      help: Default `publisher_id` for outgoing notifications. If left undefined,
        Keystone will default to using the server's host name.
      max: null
      metavar: null
      min: null
      mutable: false
      name: default_publisher_id
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices:
      - - basic
        - null
      - - cadf
        - null
      default: cadf
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: notification_format
      help: Define the notification format for identity service events. A `basic`
        notification only has information about the resource being operated on. A
        `cadf` notification has the same information, as well as information about
        the initiator of the event. The `cadf` option is entirely backwards compatible
        with the `basic` option, but is fully CADF-compliant, and is recommended for
        auditing use cases.
      max: null
      metavar: null
      min: null
      mutable: false
      name: notification_format
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default:
      - identity.authenticate.success
      - identity.authenticate.pending
      - identity.authenticate.failed
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: notification_opt_out
      help: 'You can reduce the number of notifications keystone emits by explicitly
        opting out. Keystone will not emit notifications that match the patterns expressed
        in this list. Values are expected to be in the form of `identity.<resource_type>.<operation>`.
        By default, all notifications related to authentication are automatically
        suppressed. This field can be set multiple times in order to opt-out of multiple
        notification topics. For example, the following suppresses notifications describing
        user creation or successful authentication events: notification_opt_out=identity.user.create
        notification_opt_out=identity.authenticate.success'
      max: null
      metavar: null
      min: null
      mutable: false
      name: notification_opt_out
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: multi valued
    - advanced: false
      choices: []
      default: 30
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: rpc_conn_pool_size
      deprecated_reason: null
      deprecated_since: null
      dest: rpc_conn_pool_size
      help: Size of RPC connection pool.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rpc_conn_pool_size
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 2
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: conn_pool_min_size
      help: The pool size limit for connections expiration policy
      max: null
      metavar: null
      min: null
      mutable: false
      name: conn_pool_min_size
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 1200
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: conn_pool_ttl
      help: The time-to-live in sec of idle connections in the pool
      max: null
      metavar: null
      min: null
      mutable: false
      name: conn_pool_ttl
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 64
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: rpc_thread_pool_size
      deprecated_reason: null
      deprecated_since: null
      dest: executor_thread_pool_size
      help: Size of executor thread pool when executor is threading or eventlet.
      max: null
      metavar: null
      min: null
      mutable: false
      name: executor_thread_pool_size
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 60
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rpc_response_timeout
      help: Seconds to wait for a response from a call.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rpc_response_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: rabbit://
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: transport_url
      help: 'The network address and optional user credentials for connecting to the
        messaging backend, in URL format. The expected format is:


        driver://[user:pass@]host:port[,[userN:passN@]hostN:portN]/virtual_host?query


        Example: rabbit://rabbitmq:password@127.0.0.1:5672//


        For full details on the fields in the URL see the documentation of oslo_messaging.TransportURL
        at https://docs.openstack.org/oslo.messaging/latest/reference/transport.html'
      max: null
      metavar: null
      min: null
      mutable: false
      name: transport_url
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: string value
    - advanced: false
      choices: []
      default: keystone
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: control_exchange
      help: The default exchange under which topics are scoped. May be overridden
        by an exchange name specified in the transport_url option.
      max: null
      metavar: null
      min: null
      mutable: false
      name: control_exchange
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: debug
      help: If set to true, the logging level will be set to DEBUG instead of the
        default INFO level.
      max: null
      metavar: null
      min: null
      mutable: true
      name: debug
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: d
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: log_config
      deprecated_reason: null
      deprecated_since: null
      dest: log_config_append
      help: The name of a logging configuration file. This file is appended to any
        existing logging configuration files. For details about logging configuration
        files, see the Python logging module documentation. Note that when logging
        configuration files are used then all logging configuration is set in the
        configuration file and other logging configuration options are ignored (for
        example, log-date-format).
      max: null
      metavar: PATH
      min: null
      mutable: true
      name: log-config-append
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: '%Y-%m-%d %H:%M:%S'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: log_date_format
      help: 'Defines the format string for %%(asctime)s in log records. Default: %(default)s
        . This option is ignored if log_config_append is set.'
      max: null
      metavar: DATE_FORMAT
      min: null
      mutable: false
      name: log-date-format
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: logfile
      deprecated_reason: null
      deprecated_since: null
      dest: log_file
      help: (Optional) Name of log file to send logging output to. If no default is
        set, logging will go to stderr as defined by use_stderr. This option is ignored
        if log_config_append is set.
      max: null
      metavar: PATH
      min: null
      mutable: false
      name: log-file
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: logdir
      deprecated_reason: null
      deprecated_since: null
      dest: log_dir
      help: (Optional) The base directory used for relative log_file  paths. This
        option is ignored if log_config_append is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: log-dir
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: watch_log_file
      help: Uses logging handler designed to watch file system. When log file is moved
        or removed this handler will open a new log file with specified path instantaneously.
        It makes sense only if log_file option is specified and Linux platform is
        used. This option is ignored if log_config_append is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: watch-log-file
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_syslog
      help: Use syslog for logging. Existing syslog format is DEPRECATED and will
        be changed later to honor RFC5424. This option is ignored if log_config_append
        is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use-syslog
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_journal
      help: Enable journald for logging. If running in a systemd environment you may
        wish to enable journal support. Doing so will use the journal native protocol
        which includes structured metadata in addition to log messages.This option
        is ignored if log_config_append is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use-journal
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: LOG_USER
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: syslog_log_facility
      help: Syslog facility to receive log lines. This option is ignored if log_config_append
        is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: syslog-log-facility
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_json
      help: Use JSON formatting for logging. This option is ignored if log_config_append
        is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use-json
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_stderr
      help: Log output to standard error. This option is ignored if log_config_append
        is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use_stderr
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_eventlog
      help: Log output to Windows Event Log.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use_eventlog
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: log_rotate_interval
      help: The amount of time before the log files are rotated. This option is ignored
        unless log_rotation_type is setto "interval".
      max: null
      metavar: null
      min: null
      mutable: false
      name: log_rotate_interval
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - Seconds
        - null
      - - Minutes
        - null
      - - Hours
        - null
      - - Days
        - null
      - - Weekday
        - null
      - - Midnight
        - null
      default: days
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: log_rotate_interval_type
      help: Rotation interval type. The time of the last file change (or the time
        when the service was started) is used when scheduling the next rotation.
      max: null
      metavar: null
      min: null
      mutable: false
      name: log_rotate_interval_type
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 30
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_logfile_count
      help: Maximum number of rotated log files.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_logfile_count
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 200
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_logfile_size_mb
      help: Log file maximum size in MB. This option is ignored if "log_rotation_type"
        is not set to "size".
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_logfile_size_mb
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - interval
        - Rotate logs at predefined time intervals.
      - - size
        - Rotate logs once they reach a predefined size.
      - - none
        - Do not rotate log files.
      default: none
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: log_rotation_type
      help: Log rotation type.
      max: null
      metavar: null
      min: null
      mutable: false
      name: log_rotation_type
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: '%(asctime)s.%(msecs)03d %(process)d %(levelname)s %(name)s [%(request_id)s
        %(user_identity)s] %(instance)s%(message)s'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: logging_context_format_string
      help: Format string to use for log messages with context. Used by oslo_log.formatters.ContextFormatter
      max: null
      metavar: null
      min: null
      mutable: false
      name: logging_context_format_string
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: '%(asctime)s.%(msecs)03d %(process)d %(levelname)s %(name)s [-] %(instance)s%(message)s'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: logging_default_format_string
      help: Format string to use for log messages when context is undefined. Used
        by oslo_log.formatters.ContextFormatter
      max: null
      metavar: null
      min: null
      mutable: false
      name: logging_default_format_string
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: '%(funcName)s %(pathname)s:%(lineno)d'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: logging_debug_format_suffix
      help: Additional data to append to log message when logging level for the message
        is DEBUG. Used by oslo_log.formatters.ContextFormatter
      max: null
      metavar: null
      min: null
      mutable: false
      name: logging_debug_format_suffix
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: '%(asctime)s.%(msecs)03d %(process)d ERROR %(name)s %(instance)s'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: logging_exception_prefix
      help: Prefix each line of exception output with this format. Used by oslo_log.formatters.ContextFormatter
      max: null
      metavar: null
      min: null
      mutable: false
      name: logging_exception_prefix
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: '%(user)s %(tenant)s %(domain)s %(user_domain)s %(project_domain)s'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: logging_user_identity_format
      help: Defines the format string for %(user_identity)s that is used in logging_context_format_string.
        Used by oslo_log.formatters.ContextFormatter
      max: null
      metavar: null
      min: null
      mutable: false
      name: logging_user_identity_format
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default:
      - amqp=WARN
      - amqplib=WARN
      - boto=WARN
      - qpid=WARN
      - sqlalchemy=WARN
      - suds=INFO
      - oslo.messaging=INFO
      - oslo_messaging=INFO
      - iso8601=WARN
      - requests.packages.urllib3.connectionpool=WARN
      - urllib3.connectionpool=WARN
      - websocket=WARN
      - requests.packages.urllib3.util.retry=WARN
      - urllib3.util.retry=WARN
      - keystonemiddleware=WARN
      - routes.middleware=WARN
      - stevedore=WARN
      - taskflow=WARN
      - keystoneauth=WARN
      - oslo.cache=INFO
      - oslo_policy=INFO
      - dogpile.core.dogpile=INFO
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_log_levels
      help: List of package logging levels in logger=LEVEL pairs. This option is ignored
        if log_config_append is set.
      max: null
      metavar: null
      min: null
      mutable: false
      name: default_log_levels
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: publish_errors
      help: Enables or disables publication of error events.
      max: null
      metavar: null
      min: null
      mutable: false
      name: publish_errors
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: '[instance: %(uuid)s] '
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: instance_format
      help: The format for an instance that is passed with the log message.
      max: null
      metavar: null
      min: null
      mutable: false
      name: instance_format
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: '[instance: %(uuid)s] '
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: instance_uuid_format
      help: The format for an instance UUID that is passed with the log message.
      max: null
      metavar: null
      min: null
      mutable: false
      name: instance_uuid_format
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rate_limit_interval
      help: Interval, number of seconds, of log rate limiting.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rate_limit_interval
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rate_limit_burst
      help: Maximum number of logged messages per rate_limit_interval.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rate_limit_burst
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: CRITICAL
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rate_limit_except_level
      help: 'Log level name used by rate limiting: CRITICAL, ERROR, INFO, WARNING,
        DEBUG or empty string. Logs with level greater or equal to rate_limit_except_level
        are not filtered. An empty string means that all levels are filtered.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: rate_limit_except_level
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: fatal_deprecations
      help: Enables or disables fatal status of deprecations.
      max: null
      metavar: null
      min: null
      mutable: false
      name: fatal_deprecations
      namespace: oslo.log
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - admin_token
    - public_endpoint
    - max_project_tree_depth
    - max_param_size
    - max_token_size
    - list_limit
    - strict_password_check
    - insecure_debug
    - default_publisher_id
    - notification_format
    - notification_opt_out
    - rpc_conn_pool_size
    - conn_pool_min_size
    - conn_pool_ttl
    - executor_thread_pool_size
    - rpc_response_timeout
    - transport_url
    - control_exchange
    - debug
    - log-config-append
    - log-date-format
    - log-file
    - log-dir
    - watch-log-file
    - use-syslog
    - use-journal
    - syslog-log-facility
    - use-json
    - use_stderr
    - use_eventlog
    - log_rotate_interval
    - log_rotate_interval_type
    - max_logfile_count
    - max_logfile_size_mb
    - log_rotation_type
    - logging_context_format_string
    - logging_default_format_string
    - logging_debug_format_suffix
    - logging_exception_prefix
    - logging_user_identity_format
    - default_log_levels
    - publish_errors
    - instance_format
    - instance_uuid_format
    - rate_limit_interval
    - rate_limit_burst
    - rate_limit_except_level
    - fatal_deprecations
  application_credential:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the application credential backend driver in the `keystone.application_credential`
        namespace.  Keystone only provides a `sql` driver, so there is no reason to
        change this unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for application credential caching. This has no effect unless global
        caching is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache application credential data in seconds. This has no effect
        unless global caching is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: -1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_limit
      help: Maximum number of application credentials a user is permitted to create.
        A value of -1 means unlimited. If a limit is not set, users are permitted
        to create application credentials at will, which could lead to bloat in the
        keystone database or open keystone to a DoS attack.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - caching
    - cache_time
    - user_limit
  assignment:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the assignment backend driver (where role assignments
        are stored) in the `keystone.assignment` namespace. Only a SQL driver is supplied
        by keystone itself. Unless you are writing proprietary drivers for keystone,
        you do not need to set this option.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default:
      - admin
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: prohibited_implied_role
      help: A list of role names which are prohibited from being an implied role.
      max: null
      metavar: null
      min: null
      mutable: false
      name: prohibited_implied_role
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    standard_opts:
    - driver
    - prohibited_implied_role
  auth:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default:
      - external
      - password
      - token
      - oauth1
      - mapped
      - application_credential
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: methods
      help: 'Allowed authentication methods. Note: You should disable the `external`
        auth method if you are currently using federation. External auth and federation
        both use the REMOTE_USER variable. Since both the mapped and external plugin
        are being invoked to validate attributes in the request environment, it can
        cause conflicts.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: methods
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: password
      help: Entry point for the password auth plugin module in the `keystone.auth.password`
        namespace. You do not need to set this unless you are overriding keystone's
        own password authentication plugin.
      max: null
      metavar: null
      min: null
      mutable: false
      name: password
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: token
      help: Entry point for the token auth plugin module in the `keystone.auth.token`
        namespace. You do not need to set this unless you are overriding keystone's
        own token authentication plugin.
      max: null
      metavar: null
      min: null
      mutable: false
      name: token
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: external
      help: Entry point for the external (`REMOTE_USER`) auth plugin module in the
        `keystone.auth.external` namespace. Supplied drivers are `DefaultDomain` and
        `Domain`. The default driver is `DefaultDomain`, which assumes that all users
        identified by the username specified to keystone in the `REMOTE_USER` variable
        exist within the context of the default domain. The `Domain` option expects
        an additional environment variable be presented to keystone, `REMOTE_DOMAIN`,
        containing the domain name of the `REMOTE_USER` (if `REMOTE_DOMAIN` is not
        set, then the default domain will be used instead). You do not need to set
        this unless you are taking advantage of "external authentication", where the
        application server (such as Apache) is handling authentication instead of
        keystone.
      max: null
      metavar: null
      min: null
      mutable: false
      name: external
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: oauth1
      help: Entry point for the OAuth 1.0a auth plugin module in the `keystone.auth.oauth1`
        namespace. You do not need to set this unless you are overriding keystone's
        own `oauth1` authentication plugin.
      max: null
      metavar: null
      min: null
      mutable: false
      name: oauth1
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: mapped
      help: Entry point for the mapped auth plugin module in the `keystone.auth.mapped`
        namespace. You do not need to set this unless you are overriding keystone's
        own `mapped` authentication plugin.
      max: null
      metavar: null
      min: null
      mutable: false
      name: mapped
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: application_credential
      help: Entry point for the application_credential auth plugin module in the `keystone.auth.application_credential`
        namespace. You do not need to set this unless you are overriding keystone's
        own `application_credential` authentication plugin.
      max: null
      metavar: null
      min: null
      mutable: false
      name: application_credential
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - methods
    - password
    - token
    - external
    - oauth1
    - mapped
    - application_credential
  cache:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: cache.oslo
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: config_prefix
      help: Prefix for building the configuration dictionary for the cache region.
        This should not need to be changed unless there is another dogpile.cache region
        with the same configuration name.
      max: null
      metavar: null
      min: null
      mutable: false
      name: config_prefix
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 600
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: expiration_time
      help: Default TTL, in seconds, for any cached item in the dogpile.cache region.
        This applies to any cached method that doesn't have an explicit cache expiration
        time defined for it.
      max: null
      metavar: null
      min: null
      mutable: false
      name: expiration_time
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - oslo_cache.memcache_pool
        - null
      - - oslo_cache.dict
        - null
      - - oslo_cache.mongo
        - null
      - - oslo_cache.etcd3gw
        - null
      - - dogpile.cache.memcached
        - null
      - - dogpile.cache.pylibmc
        - null
      - - dogpile.cache.bmemcached
        - null
      - - dogpile.cache.dbm
        - null
      - - dogpile.cache.redis
        - null
      - - dogpile.cache.memory
        - null
      - - dogpile.cache.memory_pickle
        - null
      - - dogpile.cache.null
        - null
      default: dogpile.cache.null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: backend
      help: Cache backend module. For eventlet-based or environments with hundreds
        of threaded servers, Memcache with pooling (oslo_cache.memcache_pool) is recommended.
        For environments with less than 100 threaded servers, Memcached (dogpile.cache.memcached)
        or Redis (dogpile.cache.redis) is recommended. Test environments with a single
        instance of the server can use the dogpile.cache.memory backend.
      max: null
      metavar: null
      min: null
      mutable: false
      name: backend
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: backend_argument
      help: 'Arguments supplied to the backend module. Specify this option once per
        argument to be passed to the dogpile.cache backend. Example format: "<argname>:<value>".'
      max: null
      metavar: null
      min: null
      mutable: false
      name: backend_argument
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: multi valued
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: proxies
      help: Proxy classes to import that will affect the way the dogpile.cache backend
        functions. See the dogpile.cache documentation on changing-backend-behavior.
      max: null
      metavar: null
      min: null
      mutable: false
      name: proxies
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: enabled
      help: Global toggle for caching.
      max: null
      metavar: null
      min: null
      mutable: false
      name: enabled
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: debug_cache_backend
      help: Extra debugging from the cache backend (cache keys, get/set/delete/etc
        calls). This is only really useful if you need to see the specific cache-backend
        get/set/delete calls with the keys/values.  Typically this should be left
        set to false.
      max: null
      metavar: null
      min: null
      mutable: false
      name: debug_cache_backend
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default:
      - localhost:11211
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: memcache_servers
      help: Memcache servers in the format of "host:port". (dogpile.cache.memcached
        and oslo_cache.memcache_pool backends only).
      max: null
      metavar: null
      min: null
      mutable: false
      name: memcache_servers
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: 300
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: memcache_dead_retry
      help: Number of seconds memcached server is considered dead before it is tried
        again. (dogpile.cache.memcache and oslo_cache.memcache_pool backends only).
      max: null
      metavar: null
      min: null
      mutable: false
      name: memcache_dead_retry
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 1.0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: memcache_socket_timeout
      help: Timeout in seconds for every call to a server. (dogpile.cache.memcache
        and oslo_cache.memcache_pool backends only).
      max: null
      metavar: null
      min: null
      mutable: false
      name: memcache_socket_timeout
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: floating point value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: memcache_pool_maxsize
      help: Max total number of open connections to every memcached server. (oslo_cache.memcache_pool
        backend only).
      max: null
      metavar: null
      min: null
      mutable: false
      name: memcache_pool_maxsize
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 60
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: memcache_pool_unused_timeout
      help: Number of seconds a connection to memcached is held unused in the pool
        before it is closed. (oslo_cache.memcache_pool backend only).
      max: null
      metavar: null
      min: null
      mutable: false
      name: memcache_pool_unused_timeout
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: memcache_pool_connection_get_timeout
      help: Number of seconds that an operation will wait to get a memcache client
        connection.
      max: null
      metavar: null
      min: null
      mutable: false
      name: memcache_pool_connection_get_timeout
      namespace: oslo.cache
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - config_prefix
    - expiration_time
    - backend
    - backend_argument
    - proxies
    - enabled
    - debug_cache_backend
    - memcache_servers
    - memcache_dead_retry
    - memcache_socket_timeout
    - memcache_pool_maxsize
    - memcache_pool_unused_timeout
    - memcache_pool_connection_get_timeout
  catalog:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: default_catalog.templates
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: template_file
      help: Absolute path to the file used for the templated catalog backend. This
        option is only used if the `[catalog] driver` is set to `templated`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: template_file
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the catalog driver in the `keystone.catalog` namespace.
        Keystone provides a `sql` option (which supports basic CRUD operations through
        SQL), a `templated` option (which loads the catalog from a templated catalog
        file on disk), and a `endpoint_filter.sql` option (which supports arbitrary
        service catalogs per project).
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for catalog caching. This has no effect unless global caching is
        enabled. In a typical deployment, there is no reason to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache catalog data (in seconds). This has no effect unless global
        and catalog caching are both enabled. Catalog data (services, endpoints, etc.)
        typically does not change frequently, and so a longer duration than the global
        default may be desirable.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: list_limit
      help: Maximum number of entities that will be returned in a catalog collection.
        There is typically no reason to set this, as it would be unusual for a deployment
        to have enough services or endpoints to exceed a reasonable limit.
      max: null
      metavar: null
      min: null
      mutable: false
      name: list_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - template_file
    - driver
    - caching
    - cache_time
    - list_limit
  cors:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: allowed_origin
      help: 'Indicate whether this resource may be shared with the domain received
        in the requests "origin" header. Format: "<protocol>://<host>[:<port>]", no
        trailing slash. Example: https://horizon.example.com'
      max: null
      metavar: null
      min: null
      mutable: false
      name: allowed_origin
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: allow_credentials
      help: Indicate that the actual request can include user credentials
      max: null
      metavar: null
      min: null
      mutable: false
      name: allow_credentials
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default:
      - X-Auth-Token
      - X-Openstack-Request-Id
      - X-Subject-Token
      - Openstack-Auth-Receipt
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: expose_headers
      help: Indicate which headers are safe to expose to the API. Defaults to HTTP
        Simple Headers.
      max: null
      metavar: null
      min: null
      mutable: false
      name: expose_headers
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: 3600
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_age
      help: Maximum cache age of CORS preflight requests.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_age
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default:
      - GET
      - PUT
      - POST
      - DELETE
      - PATCH
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: allow_methods
      help: Indicate which methods can be used during the actual request.
      max: null
      metavar: null
      min: null
      mutable: false
      name: allow_methods
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default:
      - X-Auth-Token
      - X-Openstack-Request-Id
      - X-Subject-Token
      - X-Project-Id
      - X-Project-Name
      - X-Project-Domain-Id
      - X-Project-Domain-Name
      - X-Domain-Id
      - X-Domain-Name
      - Openstack-Auth-Receipt
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: allow_headers
      help: Indicate which header field names may be used during the actual request.
      max: null
      metavar: null
      min: null
      mutable: false
      name: allow_headers
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    standard_opts:
    - allowed_origin
    - allow_credentials
    - expose_headers
    - max_age
    - allow_methods
    - allow_headers
  credential:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the credential backend driver in the `keystone.credential`
        namespace. Keystone only provides a `sql` driver, so there's no reason to
        change this unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: fernet
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: provider
      help: Entry point for credential encryption and decryption operations in the
        `keystone.credential.provider` namespace. Keystone only provides a `fernet`
        driver, so there's no reason to change this unless you are providing a custom
        entry point to encrypt and decrypt credentials.
      max: null
      metavar: null
      min: null
      mutable: false
      name: provider
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: /etc/keystone/credential-keys/
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: key_repository
      help: Directory containing Fernet keys used to encrypt and decrypt credentials
        stored in the credential backend. Fernet keys used to encrypt credentials
        have no relationship to Fernet keys used to encrypt Fernet tokens. Both sets
        of keys should be managed separately and require different rotation policies.
        Do not share this repository with the repository used to manage keys for Fernet
        tokens.
      max: null
      metavar: null
      min: null
      mutable: false
      name: key_repository
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for caching only on retrieval of user credentials. This has no
        effect unless global caching is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache credential data in seconds. This has no effect unless global
        caching is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 15
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: auth_ttl
      help: The length of time in minutes for which a signed EC2 or S3 token request
        is valid from the timestamp contained in the token request.
      max: null
      metavar: null
      min: null
      mutable: false
      name: auth_ttl
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - provider
    - key_repository
    - caching
    - cache_time
    - auth_ttl
  database:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sqlite_synchronous
      deprecated_reason: null
      deprecated_since: null
      dest: sqlite_synchronous
      help: If True, SQLite uses synchronous mode.
      max: null
      metavar: null
      min: null
      mutable: false
      name: sqlite_synchronous
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: sqlalchemy
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: db_backend
      deprecated_reason: null
      deprecated_since: null
      dest: backend
      help: The back end to use for the database.
      max: null
      metavar: null
      min: null
      mutable: false
      name: backend
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sql_connection
      - group: DATABASE
        name: sql_connection
      - group: sql
        name: connection
      deprecated_reason: null
      deprecated_since: null
      dest: connection
      help: The SQLAlchemy connection string to use to connect to the database.
      max: null
      metavar: null
      min: null
      mutable: false
      name: connection
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: slave_connection
      help: The SQLAlchemy connection string to use to connect to the slave database.
      max: null
      metavar: null
      min: null
      mutable: false
      name: slave_connection
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: string value
    - advanced: false
      choices: []
      default: TRADITIONAL
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: mysql_sql_mode
      help: 'The SQL mode to be used for MySQL sessions. This option, including the
        default, overrides any server-set SQL mode. To use whatever SQL mode is set
        by the server configuration, set this to no value. Example: mysql_sql_mode='
      max: null
      metavar: null
      min: null
      mutable: false
      name: mysql_sql_mode
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: mysql_enable_ndb
      help: If True, transparently enables support for handling MySQL Cluster (NDB).
      max: null
      metavar: null
      min: null
      mutable: false
      name: mysql_enable_ndb
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 3600
      deprecated_for_removal: false
      deprecated_opts:
      - group: DATABASE
        name: idle_timeout
      - group: database
        name: idle_timeout
      - group: DEFAULT
        name: sql_idle_timeout
      - group: DATABASE
        name: sql_idle_timeout
      - group: sql
        name: idle_timeout
      deprecated_reason: null
      deprecated_since: null
      dest: connection_recycle_time
      help: Connections which have been present in the connection pool longer than
        this number of seconds will be replaced with a new one the next time they
        are checked out from the pool.
      max: null
      metavar: null
      min: null
      mutable: false
      name: connection_recycle_time
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 5
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sql_max_pool_size
      - group: DATABASE
        name: sql_max_pool_size
      deprecated_reason: null
      deprecated_since: null
      dest: max_pool_size
      help: Maximum number of SQL connections to keep open in a pool. Setting a value
        of 0 indicates no limit.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_pool_size
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sql_max_retries
      - group: DATABASE
        name: sql_max_retries
      deprecated_reason: null
      deprecated_since: null
      dest: max_retries
      help: Maximum number of database connection retries during startup. Set to -1
        to specify an infinite retry count.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_retries
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sql_retry_interval
      - group: DATABASE
        name: reconnect_interval
      deprecated_reason: null
      deprecated_since: null
      dest: retry_interval
      help: Interval between retries of opening a SQL connection.
      max: null
      metavar: null
      min: null
      mutable: false
      name: retry_interval
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 50
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sql_max_overflow
      - group: DATABASE
        name: sqlalchemy_max_overflow
      deprecated_reason: null
      deprecated_since: null
      dest: max_overflow
      help: If set, use this value for max_overflow with SQLAlchemy.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_overflow
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sql_connection_debug
      deprecated_reason: null
      deprecated_since: null
      dest: connection_debug
      help: 'Verbosity of SQL debugging information: 0=None, 100=Everything.'
      max: 100
      metavar: null
      min: 0
      mutable: false
      name: connection_debug
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: sql_connection_trace
      deprecated_reason: null
      deprecated_since: null
      dest: connection_trace
      help: Add Python stack traces to SQL as comment strings.
      max: null
      metavar: null
      min: null
      mutable: false
      name: connection_trace
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: DATABASE
        name: sqlalchemy_pool_timeout
      deprecated_reason: null
      deprecated_since: null
      dest: pool_timeout
      help: If set, use this value for pool_timeout with SQLAlchemy.
      max: null
      metavar: null
      min: null
      mutable: false
      name: pool_timeout
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_db_reconnect
      help: Enable the experimental use of database reconnect on connection lost.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use_db_reconnect
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: db_retry_interval
      help: Seconds between retries of a database transaction.
      max: null
      metavar: null
      min: null
      mutable: false
      name: db_retry_interval
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: db_inc_retry_interval
      help: If True, increases the interval between retries of a database operation
        up to db_max_retry_interval.
      max: null
      metavar: null
      min: null
      mutable: false
      name: db_inc_retry_interval
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: db_max_retry_interval
      help: If db_inc_retry_interval is set, the maximum seconds between retries of
        a database operation.
      max: null
      metavar: null
      min: null
      mutable: false
      name: db_max_retry_interval
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 20
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: db_max_retries
      help: Maximum retries in case of connection error or deadlock error before error
        is raised. Set to -1 to specify an infinite retry count.
      max: null
      metavar: null
      min: null
      mutable: false
      name: db_max_retries
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: connection_parameters
      help: Optional URL parameters to append onto the connection URL at connect time;
        specify as param1=value1&param2=value2&...
      max: null
      metavar: null
      min: null
      mutable: false
      name: connection_parameters
      namespace: oslo.db
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - sqlite_synchronous
    - backend
    - connection
    - slave_connection
    - mysql_sql_mode
    - mysql_enable_ndb
    - connection_recycle_time
    - max_pool_size
    - max_retries
    - retry_interval
    - max_overflow
    - connection_debug
    - connection_trace
    - pool_timeout
    - use_db_reconnect
    - db_retry_interval
    - db_inc_retry_interval
    - db_max_retry_interval
    - db_max_retries
    - connection_parameters
  domain_config:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the domain-specific configuration driver in the `keystone.resource.domain_config`
        namespace. Only a `sql` option is provided by keystone, so there is no reason
        to set this unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for caching of the domain-specific configuration backend. This
        has no effect unless global caching is enabled. There is normally no reason
        to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 300
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time-to-live (TTL, in seconds) to cache domain-specific configuration
        data. This has no effect unless `[domain_config] caching` is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - caching
    - cache_time
  endpoint_filter:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the endpoint filter driver in the `keystone.endpoint_filter`
        namespace. Only a `sql` option is provided by keystone, so there is no reason
        to set this unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: return_all_endpoints_if_no_filter
      help: This controls keystone's behavior if the configured endpoint filters do
        not result in any endpoints for a user + project pair (and therefore a potentially
        empty service catalog). If set to true, keystone will return the entire service
        catalog. If set to false, keystone will return an empty service catalog.
      max: null
      metavar: null
      min: null
      mutable: false
      name: return_all_endpoints_if_no_filter
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - driver
    - return_all_endpoints_if_no_filter
  endpoint_policy:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the endpoint policy driver in the `keystone.endpoint_policy`
        namespace. Only a `sql` driver is provided by keystone, so there is no reason
        to set this unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - driver
  eventlet_server:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 0.0.0.0
      deprecated_for_removal: true
      deprecated_opts:
      - group: DEFAULT
        name: bind_host
      - group: DEFAULT
        name: public_bind_host
      deprecated_reason: Support for running keystone under eventlet has been removed
        in the Newton release. These options remain for backwards compatibility because
        they are used for URL substitutions.
      deprecated_since: K
      dest: public_bind_host
      help: The IP address of the network interface for the public service to listen
        on.
      max: null
      metavar: null
      min: null
      mutable: false
      name: public_bind_host
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: host address value
    - advanced: false
      choices: []
      default: 5000
      deprecated_for_removal: true
      deprecated_opts:
      - group: DEFAULT
        name: public_port
      deprecated_reason: Support for running keystone under eventlet has been removed
        in the Newton release. These options remain for backwards compatibility because
        they are used for URL substitutions.
      deprecated_since: K
      dest: public_port
      help: The port number for the public service to listen on.
      max: 65535
      metavar: null
      min: 0
      mutable: false
      name: public_port
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: port value
    - advanced: false
      choices: []
      default: 0.0.0.0
      deprecated_for_removal: true
      deprecated_opts:
      - group: DEFAULT
        name: bind_host
      - group: DEFAULT
        name: admin_bind_host
      deprecated_reason: Support for running keystone under eventlet has been removed
        in the Newton release. These options remain for backwards compatibility because
        they are used for URL substitutions.
      deprecated_since: K
      dest: admin_bind_host
      help: The IP address of the network interface for the admin service to listen
        on.
      max: null
      metavar: null
      min: null
      mutable: false
      name: admin_bind_host
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: host address value
    - advanced: false
      choices: []
      default: 35357
      deprecated_for_removal: true
      deprecated_opts:
      - group: DEFAULT
        name: admin_port
      deprecated_reason: Support for running keystone under eventlet has been removed
        in the Newton release. These options remain for backwards compatibility because
        they are used for URL substitutions.
      deprecated_since: K
      dest: admin_port
      help: The port number for the admin service to listen on.
      max: 65535
      metavar: null
      min: 0
      mutable: false
      name: admin_port
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: port value
    standard_opts:
    - public_bind_host
    - public_port
    - admin_bind_host
    - admin_port
  federation:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the federation backend driver in the `keystone.federation`
        namespace. Keystone only provides a `sql` driver, so there is no reason to
        set this option unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: assertion_prefix
      help: Prefix to use when filtering environment variable names for federated
        assertions. Matched variables are passed into the federated mapping engine.
      max: null
      metavar: null
      min: null
      mutable: false
      name: assertion_prefix
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: remote_id_attribute
      help: Default value for all protocols to be used to obtain the entity ID of
        the Identity Provider from the environment. For `mod_shib`, this would be
        `Shib-Identity-Provider`. For `mod_auth_openidc`, this could be `HTTP_OIDC_ISS`.
        For `mod_auth_mellon`, this could be `MELLON_IDP`. This can be overridden
        on a per-protocol basis by providing a `remote_id_attribute` to the federation
        protocol using the API.
      max: null
      metavar: null
      min: null
      mutable: false
      name: remote_id_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: Federated
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: This option has been superseded by ephemeral users existing
        in the domain of their identity provider.
      deprecated_since: T
      dest: federated_domain_name
      help: An arbitrary domain name that is reserved to allow federated ephemeral
        users to have a domain concept. Note that an admin will not be able to create
        a domain with this name or update an existing domain to this name. You are
        not advised to change this value unless you really have to.
      max: null
      metavar: null
      min: null
      mutable: false
      name: federated_domain_name
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: trusted_dashboard
      help: 'A list of trusted dashboard hosts. Before accepting a Single Sign-On
        request to return a token, the origin host must be a member of this list.
        This configuration option may be repeated for multiple values. You must set
        this in order to use web-based SSO flows. For example: trusted_dashboard=https://acme.example.com/auth/websso
        trusted_dashboard=https://beta.example.com/auth/websso'
      max: null
      metavar: null
      min: null
      mutable: false
      name: trusted_dashboard
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: multi valued
    - advanced: false
      choices: []
      default: /etc/keystone/sso_callback_template.html
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: sso_callback_template
      help: Absolute path to an HTML file used as a Single Sign-On callback handler.
        This page is expected to redirect the user from keystone back to a trusted
        dashboard host, by form encoding a token in a POST request. Keystone's default
        value should be sufficient for most deployments.
      max: null
      metavar: null
      min: null
      mutable: false
      name: sso_callback_template
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for federation caching. This has no effect unless global caching
        is enabled. There is typically no reason to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_authorization_ttl
      help: Default time in minutes for the validity of group memberships carried
        over from a mapping. Default is 0, which means disabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: default_authorization_ttl
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - assertion_prefix
    - remote_id_attribute
    - federated_domain_name
    - trusted_dashboard
    - sso_callback_template
    - caching
    - default_authorization_ttl
  fernet_receipts:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: /etc/keystone/fernet-keys/
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: key_repository
      help: 'Directory containing Fernet receipt keys. This directory must exist before
        using `keystone-manage fernet_setup` for the first time, must be writable
        by the user running `keystone-manage fernet_setup` or `keystone-manage fernet_rotate`,
        and of course must be readable by keystone''s server process. The repository
        may contain keys in one of three states: a single staged key (always index
        0) used for receipt validation, a single primary key (always the highest index)
        used for receipt creation and validation, and any number of secondary keys
        (all other index values) used for receipt validation. With multiple keystone
        nodes, each node must share the same key repository contents, with the exception
        of the staged key (index 0). It is safe to run `keystone-manage fernet_rotate`
        once on any one node to promote a staged key (index 0) to be the new primary
        (incremented from the previous highest index), and produce a new staged key
        (a new key with index 0); the resulting repository can then be atomically
        replicated to other nodes without any risk of race conditions (for example,
        it is safe to run `keystone-manage fernet_rotate` on host A, wait any amount
        of time, create a tarball of the directory on host A, unpack it on host B
        to a temporary location, and atomically move (`mv`) the directory into place
        on host B). Running `keystone-manage fernet_rotate` *twice* on a key repository
        without syncing other nodes will result in receipts that can not be validated
        by all nodes.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: key_repository
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 3
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_active_keys
      help: This controls how many keys are held in rotation by `keystone-manage fernet_rotate`
        before they are discarded. The default value of 3 means that keystone will
        maintain one staged key (always index 0), one primary key (the highest numerical
        index), and one secondary key (every other index). Increasing this value means
        that additional secondary keys will be kept in the rotation.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: max_active_keys
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - key_repository
    - max_active_keys
  fernet_tokens:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: /etc/keystone/fernet-keys/
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: key_repository
      help: 'Directory containing Fernet token keys. This directory must exist before
        using `keystone-manage fernet_setup` for the first time, must be writable
        by the user running `keystone-manage fernet_setup` or `keystone-manage fernet_rotate`,
        and of course must be readable by keystone''s server process. The repository
        may contain keys in one of three states: a single staged key (always index
        0) used for token validation, a single primary key (always the highest index)
        used for token creation and validation, and any number of secondary keys (all
        other index values) used for token validation. With multiple keystone nodes,
        each node must share the same key repository contents, with the exception
        of the staged key (index 0). It is safe to run `keystone-manage fernet_rotate`
        once on any one node to promote a staged key (index 0) to be the new primary
        (incremented from the previous highest index), and produce a new staged key
        (a new key with index 0); the resulting repository can then be atomically
        replicated to other nodes without any risk of race conditions (for example,
        it is safe to run `keystone-manage fernet_rotate` on host A, wait any amount
        of time, create a tarball of the directory on host A, unpack it on host B
        to a temporary location, and atomically move (`mv`) the directory into place
        on host B). Running `keystone-manage fernet_rotate` *twice* on a key repository
        without syncing other nodes will result in tokens that can not be validated
        by all nodes.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: key_repository
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 3
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_active_keys
      help: This controls how many keys are held in rotation by `keystone-manage fernet_rotate`
        before they are discarded. The default value of 3 means that keystone will
        maintain one staged key (always index 0), one primary key (the highest numerical
        index), and one secondary key (every other index). Increasing this value means
        that additional secondary keys will be kept in the rotation.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: max_active_keys
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - key_repository
    - max_active_keys
  healthcheck:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: /healthcheck
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: path
      help: The path to respond to healtcheck requests on.
      max: null
      metavar: null
      min: null
      mutable: false
      name: path
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: detailed
      help: 'Show more detailed information as part of the response. Security note:
        Enabling this option may expose sensitive details about the service being
        monitored. Be sure to verify that it will not violate your security policies.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: detailed
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: backends
      help: Additional backends that can perform health checks and report that information
        back as part of a request.
      max: null
      metavar: null
      min: null
      mutable: false
      name: backends
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: disable_by_file_path
      help: Check the presence of a file to determine if an application is running
        on a port. Used by DisableByFileHealthcheck plugin.
      max: null
      metavar: null
      min: null
      mutable: false
      name: disable_by_file_path
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: disable_by_file_paths
      help: Check the presence of a file based on a port to determine if an application
        is running on a port. Expects a "port:path" list of strings. Used by DisableByFilesPortsHealthcheck
        plugin.
      max: null
      metavar: null
      min: null
      mutable: false
      name: disable_by_file_paths
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    standard_opts:
    - path
    - detailed
    - backends
    - disable_by_file_path
    - disable_by_file_paths
  identity:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: default
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_domain_id
      help: This references the domain to use for all Identity API v2 requests (which
        are not aware of domains). A domain with this ID can optionally be created
        for you by `keystone-manage bootstrap`. The domain referenced by this ID cannot
        be deleted on the v3 API, to prevent accidentally breaking the v2 API. There
        is nothing special about this domain, other than the fact that it must exist
        to order to maintain support for your v2 clients. There is typically no reason
        to change this value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: default_domain_id
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: domain_specific_drivers_enabled
      help: A subset (or all) of domains can have their own identity driver, each
        with their own partial configuration options, stored in either the resource
        backend or in a file in a domain configuration directory (depending on the
        setting of `[identity] domain_configurations_from_database`). Only values
        specific to the domain need to be specified in this manner. This feature is
        disabled by default, but may be enabled by default in a future release; set
        to true to enable.
      max: null
      metavar: null
      min: null
      mutable: false
      name: domain_specific_drivers_enabled
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: domain_configurations_from_database
      help: By default, domain-specific configuration data is read from files in the
        directory identified by `[identity] domain_config_dir`. Enabling this configuration
        option allows you to instead manage domain-specific configurations through
        the API, which are then persisted in the backend (typically, a SQL database),
        rather than using configuration files on disk.
      max: null
      metavar: null
      min: null
      mutable: false
      name: domain_configurations_from_database
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: /etc/keystone/domains
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: domain_config_dir
      help: Absolute path where keystone should locate domain-specific `[identity]`
        configuration files. This option has no effect unless `[identity] domain_specific_drivers_enabled`
        is set to true. There is typically no reason to change this value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: domain_config_dir
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the identity backend driver in the `keystone.identity`
        namespace. Keystone provides a `sql` and `ldap` driver. This option is also
        used as the default driver selection (along with the other configuration variables
        in this section) in the event that `[identity] domain_specific_drivers_enabled`
        is enabled, but no applicable domain-specific configuration is defined for
        the domain in question. Unless your deployment primarily relies on `ldap`
        AND is not using domain-specific configuration, you should typically leave
        this set to `sql`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for identity caching. This has no effect unless global caching
        is enabled. There is typically no reason to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 600
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache identity data (in seconds). This has no effect unless global
        and identity caching are enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 4096
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_password_length
      help: Maximum allowed length for user passwords. Decrease this value to improve
        performance. Changing this value does not effect existing passwords.
      max: 4096
      metavar: null
      min: null
      mutable: false
      name: max_password_length
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: list_limit
      help: Maximum number of entities that will be returned in an identity collection.
      max: null
      metavar: null
      min: null
      mutable: false
      name: list_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - bcrypt
        - null
      - - scrypt
        - null
      - - pbkdf2_sha512
        - null
      default: bcrypt
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: password_hash_algorithm
      help: The password hashing algorithm to use for passwords stored within keystone.
      max: null
      metavar: null
      min: null
      mutable: false
      name: password_hash_algorithm
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: password_hash_rounds
      help: 'This option represents a trade off between security and performance.
        Higher values lead to slower performance, but higher security. Changing this
        option will only affect newly created passwords as existing password hashes
        already have a fixed number of rounds applied, so it is safe to tune this
        option in a running cluster.  The default for bcrypt is 12, must be between
        4 and 31, inclusive.  The default for scrypt is 16, must be within `range(1,32)`.  The
        default for pbkdf_sha512 is 60000, must be within `range(1,1<<32)`  WARNING:
        If using scrypt, increasing this value increases BOTH time AND memory requirements
        to hash a password.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: password_hash_rounds
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: scrypt_block_size
      help: Optional block size to pass to scrypt hash function (the `r` parameter).
        Useful for tuning scrypt to optimal performance for your CPU architecture.
        This option is only used when the `password_hash_algorithm` option is set
        to `scrypt`. Defaults to 8.
      max: null
      metavar: null
      min: null
      mutable: false
      name: scrypt_block_size
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: scrypt_parallelism
      help: Optional parallelism to pass to scrypt hash function (the `p` parameter).
        This option is only used when the `password_hash_algorithm` option is set
        to `scrypt`. Defaults to 1.
      max: null
      metavar: null
      min: null
      mutable: false
      name: scrypt_parallelism
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: salt_bytesize
      help: Number of bytes to use in scrypt and pbkfd2_sha512 hashing salt.  Default
        for scrypt is 16 bytes. Default for pbkfd2_sha512 is 16 bytes.  Limited to
        a maximum of 96 bytes due to the size of the column used to store password
        hashes.
      max: 96
      metavar: null
      min: 0
      mutable: false
      name: salt_bytesize
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - default_domain_id
    - domain_specific_drivers_enabled
    - domain_configurations_from_database
    - domain_config_dir
    - driver
    - caching
    - cache_time
    - max_password_length
    - list_limit
    - password_hash_algorithm
    - password_hash_rounds
    - scrypt_block_size
    - scrypt_parallelism
    - salt_bytesize
  identity_mapping:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the identity mapping backend driver in the `keystone.identity.id_mapping`
        namespace. Keystone only provides a `sql` driver, so there is no reason to
        change this unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: sha256
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: generator
      help: Entry point for the public ID generator for user and group entities in
        the `keystone.identity.id_generator` namespace. The Keystone identity mapper
        only supports generators that produce 64 bytes or less. Keystone only provides
        a `sha256` entry point, so there is no reason to change this value unless
        you're providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: generator
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: backward_compatible_ids
      help: The format of user and group IDs changed in Juno for backends that do
        not generate UUIDs (for example, LDAP), with keystone providing a hash mapping
        to the underlying attribute in LDAP. By default this mapping is disabled,
        which ensures that existing IDs will not change. Even when the mapping is
        enabled by using domain-specific drivers (`[identity] domain_specific_drivers_enabled`),
        any users and groups from the default domain being handled by LDAP will still
        not be mapped to ensure their IDs remain backward compatible. Setting this
        value to false will enable the new mapping for all backends, including the
        default LDAP driver. It is only guaranteed to be safe to enable this option
        if you do not already have assignments for users and groups from the default
        LDAP domain, and you consider it to be acceptable for Keystone to provide
        the different IDs to clients than it did previously (existing IDs in the API
        will suddenly change). Typically this means that the only time you can set
        this value to false is when configuring a fresh installation, although that
        is the recommended value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: backward_compatible_ids
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - driver
    - generator
    - backward_compatible_ids
  jwt_tokens:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: /etc/keystone/jws-keys/public
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: jws_public_key_repository
      help: Directory containing public keys for validating JWS token signatures.
        This directory must exist in order for keystone's server process to start.
        It must also be readable by keystone's server process. It must contain at
        least one public key that corresponds to a private key in `keystone.conf [jwt_tokens]
        jws_private_key_repository`. This option is only applicable in deployments
        issuing JWS tokens and setting `keystone.conf [token] provider = jws`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: jws_public_key_repository
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: /etc/keystone/jws-keys/private
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: jws_private_key_repository
      help: Directory containing private keys for signing JWS tokens. This directory
        must exist in order for keystone's server process to start. It must also be
        readable by keystone's server process. It must contain at least one private
        key that corresponds to a public key in `keystone.conf [jwt_tokens] jws_public_key_repository`.
        In the event there are multiple private keys in this directory, keystone will
        use a key named `private.pem` to sign tokens. In the future, keystone may
        support the ability to sign tokens with multiple private keys. For now, only
        a key named `private.pem` within this directory is required to issue JWS tokens.
        This option is only applicable in deployments issuing JWS tokens and setting
        `keystone.conf [token] provider = jws`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: jws_private_key_repository
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - jws_public_key_repository
    - jws_private_key_repository
  ldap:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: ldap://localhost
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: url
      help: URL(s) for connecting to the LDAP server. Multiple LDAP URLs may be specified
        as a comma separated string. The first URL to successfully bind is used for
        the connection.
      max: null
      metavar: null
      min: null
      mutable: false
      name: url
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user
      help: The user name of the administrator bind DN to use when querying the LDAP
        server, if your LDAP server requires it.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: password
      help: The password of the administrator bind DN to use when querying the LDAP
        server, if your LDAP server requires it.
      max: null
      metavar: null
      min: null
      mutable: false
      name: password
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: string value
    - advanced: false
      choices: []
      default: cn=example,cn=com
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: suffix
      help: The default LDAP server suffix to use, if a DN is not defined via either
        `[ldap] user_tree_dn` or `[ldap] group_tree_dn`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: suffix
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices:
      - - one
        - null
      - - sub
        - null
      default: one
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: query_scope
      help: The search scope which defines how deep to search within the search base.
        A value of `one` (representing `oneLevel` or `singleLevel`) indicates a search
        of objects immediately below to the base object, but does not include the
        base object itself. A value of `sub` (representing `subtree` or `wholeSubtree`)
        indicates a search of both the base object itself and the entire subtree below
        it.
      max: null
      metavar: null
      min: null
      mutable: false
      name: query_scope
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: page_size
      help: Defines the maximum number of results per page that keystone should request
        from the LDAP server when listing objects. A value of zero (`0`) disables
        paging.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: page_size
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - never
        - null
      - - searching
        - null
      - - always
        - null
      - - finding
        - null
      - - default
        - null
      default: default
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: alias_dereferencing
      help: The LDAP dereferencing option to use for queries involving aliases. A
        value of `default` falls back to using default dereferencing behavior configured
        by your `ldap.conf`. A value of `never` prevents aliases from being dereferenced
        at all. A value of `searching` dereferences aliases only after name resolution.
        A value of `finding` dereferences aliases only during name resolution. A value
        of `always` dereferences aliases in all cases.
      max: null
      metavar: null
      min: null
      mutable: false
      name: alias_dereferencing
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: debug_level
      help: Sets the LDAP debugging level for LDAP calls. A value of 0 means that
        debugging is not enabled. This value is a bitmask, consult your LDAP documentation
        for possible values.
      max: null
      metavar: null
      min: -1
      mutable: false
      name: debug_level
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: chase_referrals
      help: Sets keystone's referral chasing behavior across directory partitions.
        If left unset, the system's default behavior will be used.
      max: null
      metavar: null
      min: null
      mutable: false
      name: chase_referrals
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_tree_dn
      help: The search base to use for users. Defaults to the `[ldap] suffix` value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_tree_dn
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_filter
      help: The LDAP search filter to use for users.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_filter
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: inetOrgPerson
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_objectclass
      help: The LDAP object class to use for users.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_objectclass
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: cn
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_id_attribute
      help: The LDAP attribute mapped to user IDs in keystone. This must NOT be a
        multivalued attribute. User IDs are expected to be globally unique across
        keystone domains and URL-safe.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_id_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: sn
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_name_attribute
      help: The LDAP attribute mapped to user names in keystone. User names are expected
        to be unique only within a keystone domain and are not expected to be URL-safe.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_name_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: description
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_description_attribute
      help: The LDAP attribute mapped to user descriptions in keystone.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_description_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: mail
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_mail_attribute
      help: The LDAP attribute mapped to user emails in keystone.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_mail_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: userPassword
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_pass_attribute
      help: The LDAP attribute mapped to user passwords in keystone.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_pass_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: enabled
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_enabled_attribute
      help: The LDAP attribute mapped to the user enabled attribute in keystone. If
        setting this option to `userAccountControl`, then you may be interested in
        setting `[ldap] user_enabled_mask` and `[ldap] user_enabled_default` as well.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_enabled_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_enabled_invert
      help: Logically negate the boolean value of the enabled attribute obtained from
        the LDAP server. Some LDAP servers use a boolean lock attribute where "true"
        means an account is disabled. Setting `[ldap] user_enabled_invert = true`
        will allow these lock attributes to be used. This option will have no effect
        if either the `[ldap] user_enabled_mask` or `[ldap] user_enabled_emulation`
        options are in use.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_enabled_invert
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_enabled_mask
      help: Bitmask integer to select which bit indicates the enabled value if the
        LDAP server represents "enabled" as a bit on an integer rather than as a discrete
        boolean. A value of `0` indicates that the mask is not used. If this is not
        set to `0` the typical value is `2`. This is typically used when `[ldap] user_enabled_attribute
        = userAccountControl`. Setting this option causes keystone to ignore the value
        of `[ldap] user_enabled_invert`.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: user_enabled_mask
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 'True'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_enabled_default
      help: The default value to enable users. This should match an appropriate integer
        value if the LDAP server uses non-boolean (bitmask) values to indicate if
        a user is enabled or disabled. If this is not set to `True`, then the typical
        value is `512`. This is typically used when `[ldap] user_enabled_attribute
        = userAccountControl`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_enabled_default
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default:
      - default_project_id
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_attribute_ignore
      help: List of user attributes to ignore on create and update, or whether a specific
        user attribute should be filtered for list or show user.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_attribute_ignore
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_default_project_id_attribute
      help: The LDAP attribute mapped to a user's default_project_id in keystone.
        This is most commonly used when keystone has write access to LDAP.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_default_project_id_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_enabled_emulation
      help: If enabled, keystone uses an alternative method to determine if a user
        is enabled or not by checking if they are a member of the group defined by
        the `[ldap] user_enabled_emulation_dn` option. Enabling this option causes
        keystone to ignore the value of `[ldap] user_enabled_invert`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_enabled_emulation
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_enabled_emulation_dn
      help: DN of the group entry to hold enabled users when using enabled emulation.
        Setting this option has no effect unless `[ldap] user_enabled_emulation` is
        also enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_enabled_emulation_dn
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_enabled_emulation_use_group_config
      help: Use the `[ldap] group_member_attribute` and `[ldap] group_objectclass`
        settings to determine membership in the emulated enabled group. Enabling this
        option has no effect unless `[ldap] user_enabled_emulation` is also enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_enabled_emulation_use_group_config
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: user_additional_attribute_mapping
      help: A list of LDAP attribute to keystone user attribute pairs used for mapping
        additional attributes to users in keystone. The expected format is `<ldap_attr>:<user_attr>`,
        where `ldap_attr` is the attribute in the LDAP object and `user_attr` is the
        attribute which should appear in the identity API.
      max: null
      metavar: null
      min: null
      mutable: false
      name: user_additional_attribute_mapping
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_tree_dn
      help: The search base to use for groups. Defaults to the `[ldap] suffix` value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_tree_dn
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_filter
      help: The LDAP search filter to use for groups.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_filter
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: groupOfNames
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_objectclass
      help: The LDAP object class to use for groups. If setting this option to `posixGroup`,
        you may also be interested in enabling the `[ldap] group_members_are_ids`
        option.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_objectclass
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: cn
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_id_attribute
      help: The LDAP attribute mapped to group IDs in keystone. This must NOT be a
        multivalued attribute. Group IDs are expected to be globally unique across
        keystone domains and URL-safe.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_id_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ou
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_name_attribute
      help: The LDAP attribute mapped to group names in keystone. Group names are
        expected to be unique only within a keystone domain and are not expected to
        be URL-safe.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_name_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: member
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_member_attribute
      help: The LDAP attribute used to indicate that a user is a member of the group.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_member_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_members_are_ids
      help: Enable this option if the members of the group object class are keystone
        user IDs rather than LDAP DNs. This is the case when using `posixGroup` as
        the group object class in Open Directory.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_members_are_ids
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: description
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_desc_attribute
      help: The LDAP attribute mapped to group descriptions in keystone.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_desc_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_attribute_ignore
      help: List of group attributes to ignore on create and update. or whether a
        specific group attribute should be filtered for list or show group.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_attribute_ignore
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_additional_attribute_mapping
      help: A list of LDAP attribute to keystone group attribute pairs used for mapping
        additional attributes to groups in keystone. The expected format is `<ldap_attr>:<group_attr>`,
        where `ldap_attr` is the attribute in the LDAP object and `group_attr` is
        the attribute which should appear in the identity API.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_additional_attribute_mapping
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: group_ad_nesting
      help: If enabled, group queries will use Active Directory specific filters for
        nested groups.
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_ad_nesting
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: tls_cacertfile
      help: An absolute path to a CA certificate file to use when communicating with
        LDAP servers. This option will take precedence over `[ldap] tls_cacertdir`,
        so there is no reason to set both.
      max: null
      metavar: null
      min: null
      mutable: false
      name: tls_cacertfile
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: tls_cacertdir
      help: An absolute path to a CA certificate directory to use when communicating
        with LDAP servers. There is no reason to set this option if you've also set
        `[ldap] tls_cacertfile`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: tls_cacertdir
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_tls
      help: Enable TLS when communicating with LDAP servers. You should also set the
        `[ldap] tls_cacertfile` and `[ldap] tls_cacertdir` options when using this
        option. Do not set this option if you are using LDAP over SSL (LDAPS) instead
        of TLS.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use_tls
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices:
      - - demand
        - null
      - - never
        - null
      - - allow
        - null
      default: demand
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: tls_req_cert
      help: Specifies which checks to perform against client certificates on incoming
        TLS sessions. If set to `demand`, then a certificate will always be requested
        and required from the LDAP server. If set to `allow`, then a certificate will
        always be requested but not required from the LDAP server. If set to `never`,
        then a certificate will never be requested.
      max: null
      metavar: null
      min: null
      mutable: false
      name: tls_req_cert
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: -1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: connection_timeout
      help: The connection timeout to use with the LDAP server. A value of `-1` means
        that connections will never timeout.
      max: null
      metavar: null
      min: -1
      mutable: false
      name: connection_timeout
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_pool
      help: Enable LDAP connection pooling for queries to the LDAP server. There is
        typically no reason to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use_pool
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_size
      help: The size of the LDAP connection pool. This option has no effect unless
        `[ldap] use_pool` is also enabled.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: pool_size
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 3
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_retry_max
      help: The maximum number of times to attempt reconnecting to the LDAP server
        before aborting. A value of zero prevents retries. This option has no effect
        unless `[ldap] use_pool` is also enabled.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: pool_retry_max
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0.1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_retry_delay
      help: The number of seconds to wait before attempting to reconnect to the LDAP
        server. This option has no effect unless `[ldap] use_pool` is also enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: pool_retry_delay
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: floating point value
    - advanced: false
      choices: []
      default: -1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_connection_timeout
      help: The connection timeout to use when pooling LDAP connections. A value of
        `-1` means that connections will never timeout. This option has no effect
        unless `[ldap] use_pool` is also enabled.
      max: null
      metavar: null
      min: -1
      mutable: false
      name: pool_connection_timeout
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 600
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_connection_lifetime
      help: The maximum connection lifetime to the LDAP server in seconds. When this
        lifetime is exceeded, the connection will be unbound and removed from the
        connection pool. This option has no effect unless `[ldap] use_pool` is also
        enabled.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: pool_connection_lifetime
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: use_auth_pool
      help: Enable LDAP connection pooling for end user authentication. There is typically
        no reason to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: use_auth_pool
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 100
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: auth_pool_size
      help: The size of the connection pool to use for end user authentication. This
        option has no effect unless `[ldap] use_auth_pool` is also enabled.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: auth_pool_size
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 60
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: auth_pool_connection_lifetime
      help: The maximum end user authentication connection lifetime to the LDAP server
        in seconds. When this lifetime is exceeded, the connection will be unbound
        and removed from the connection pool. This option has no effect unless `[ldap]
        use_auth_pool` is also enabled.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: auth_pool_connection_lifetime
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - url
    - user
    - password
    - suffix
    - query_scope
    - page_size
    - alias_dereferencing
    - debug_level
    - chase_referrals
    - user_tree_dn
    - user_filter
    - user_objectclass
    - user_id_attribute
    - user_name_attribute
    - user_description_attribute
    - user_mail_attribute
    - user_pass_attribute
    - user_enabled_attribute
    - user_enabled_invert
    - user_enabled_mask
    - user_enabled_default
    - user_attribute_ignore
    - user_default_project_id_attribute
    - user_enabled_emulation
    - user_enabled_emulation_dn
    - user_enabled_emulation_use_group_config
    - user_additional_attribute_mapping
    - group_tree_dn
    - group_filter
    - group_objectclass
    - group_id_attribute
    - group_name_attribute
    - group_member_attribute
    - group_members_are_ids
    - group_desc_attribute
    - group_attribute_ignore
    - group_additional_attribute_mapping
    - group_ad_nesting
    - tls_cacertfile
    - tls_cacertdir
    - use_tls
    - tls_req_cert
    - connection_timeout
    - use_pool
    - pool_size
    - pool_retry_max
    - pool_retry_delay
    - pool_connection_timeout
    - pool_connection_lifetime
    - use_auth_pool
    - auth_pool_size
    - auth_pool_connection_lifetime
  memcache:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 300
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: dead_retry
      help: Number of seconds memcached server is considered dead before it is tried
        again. This is used by the key value store system.
      max: null
      metavar: null
      min: null
      mutable: false
      name: dead_retry
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 3
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: 'This option is duplicated with oslo.cache. Configure ``keystone.conf
        [cache] memcache_socket_timeout`` option to set the socket_timeout of memcached
        instead. '
      deprecated_since: T
      dest: socket_timeout
      help: Timeout in seconds for every call to a server. This is used by the key
        value store system.
      max: null
      metavar: null
      min: null
      mutable: false
      name: socket_timeout
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_maxsize
      help: Max total number of open connections to every memcached server. This is
        used by the key value store system.
      max: null
      metavar: null
      min: null
      mutable: false
      name: pool_maxsize
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 60
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_unused_timeout
      help: Number of seconds a connection to memcached is held unused in the pool
        before it is closed. This is used by the key value store system.
      max: null
      metavar: null
      min: null
      mutable: false
      name: pool_unused_timeout
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pool_connection_get_timeout
      help: Number of seconds that an operation will wait to get a memcache client
        connection. This is used by the key value store system.
      max: null
      metavar: null
      min: null
      mutable: false
      name: pool_connection_get_timeout
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - dead_retry
    - socket_timeout
    - pool_maxsize
    - pool_unused_timeout
    - pool_connection_get_timeout
  oauth1:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the OAuth backend driver in the `keystone.oauth1` namespace.
        Typically, there is no reason to set this option unless you are providing
        a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 28800
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: request_token_duration
      help: Number of seconds for the OAuth Request Token to remain valid after being
        created. This is the amount of time the user has to authorize the token. Setting
        this option to zero means that request tokens will last forever.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: request_token_duration
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 86400
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: access_token_duration
      help: Number of seconds for the OAuth Access Token to remain valid after being
        created. This is the amount of time the consumer has to interact with the
        service provider (which is typically keystone). Setting this option to zero
        means that access tokens will last forever.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: access_token_duration
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - request_token_duration
    - access_token_duration
  oslo_messaging_amqp:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: container_name
      deprecated_reason: null
      deprecated_since: null
      dest: container_name
      help: Name for the AMQP container. must be globally unique. Defaults to a generated
        UUID
      max: null
      metavar: null
      min: null
      mutable: false
      name: container_name
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: idle_timeout
      deprecated_reason: null
      deprecated_since: null
      dest: idle_timeout
      help: Timeout for inactive connections (in seconds)
      max: null
      metavar: null
      min: null
      mutable: false
      name: idle_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: trace
      deprecated_reason: null
      deprecated_since: null
      dest: trace
      help: 'Debug: dump AMQP frames to stdout'
      max: null
      metavar: null
      min: null
      mutable: false
      name: trace
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: ssl
      help: Attempt to connect via SSL. If no other ssl-related parameters are given,
        it will use the system's CA-bundle to verify the server's certificate.
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: ssl_ca_file
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_ca_file
      help: CA certificate PEM file used to verify the server's certificate
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_ca_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: ssl_cert_file
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_cert_file
      help: Self-identifying certificate PEM file for client authentication
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_cert_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: ssl_key_file
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_key_file
      help: Private key PEM file used to sign ssl_cert_file certificate (optional)
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_key_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: ssl_key_password
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_key_password
      help: Password for decrypting ssl_key_file (if encrypted)
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_key_password
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_verify_vhost
      help: By default SSL checks that the name in the server's certificate matches
        the hostname in the transport_url. In some configurations it may be preferable
        to use the virtual hostname instead, for example if the server uses the Server
        Name Indication TLS extension (rfc6066) to provide a certificate per virtual
        host. Set ssl_verify_vhost to True if the server's SSL certificate uses the
        virtual host name instead of the DNS name.
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_verify_vhost
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: sasl_mechanisms
      deprecated_reason: null
      deprecated_since: null
      dest: sasl_mechanisms
      help: Space separated list of acceptable SASL mechanisms
      max: null
      metavar: null
      min: null
      mutable: false
      name: sasl_mechanisms
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: sasl_config_dir
      deprecated_reason: null
      deprecated_since: null
      dest: sasl_config_dir
      help: Path to directory that contains the SASL configuration
      max: null
      metavar: null
      min: null
      mutable: false
      name: sasl_config_dir
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: sasl_config_name
      deprecated_reason: null
      deprecated_since: null
      dest: sasl_config_name
      help: Name of configuration file (without .conf suffix)
      max: null
      metavar: null
      min: null
      mutable: false
      name: sasl_config_name
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: sasl_default_realm
      help: SASL realm to use if no realm present in username
      max: null
      metavar: null
      min: null
      mutable: false
      name: sasl_default_realm
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: connection_retry_interval
      help: Seconds to pause before attempting to re-connect.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: connection_retry_interval
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 2
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: connection_retry_backoff
      help: Increase the connection_retry_interval by this many seconds after each
        unsuccessful failover attempt.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: connection_retry_backoff
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 30
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: connection_retry_interval_max
      help: Maximum limit for connection_retry_interval + connection_retry_backoff
      max: null
      metavar: null
      min: 1
      mutable: false
      name: connection_retry_interval_max
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: link_retry_delay
      help: Time to pause between re-connecting an AMQP 1.0 link that failed due to
        a recoverable error.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: link_retry_delay
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_reply_retry
      help: The maximum number of attempts to re-send a reply message which failed
        due to a recoverable error.
      max: null
      metavar: null
      min: -1
      mutable: false
      name: default_reply_retry
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 30
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_reply_timeout
      help: The deadline for an rpc reply message delivery.
      max: null
      metavar: null
      min: 5
      mutable: false
      name: default_reply_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 30
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_send_timeout
      help: The deadline for an rpc cast or call message delivery. Only used when
        caller does not provide a timeout expiry.
      max: null
      metavar: null
      min: 5
      mutable: false
      name: default_send_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 30
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_notify_timeout
      help: The deadline for a sent notification message delivery. Only used when
        caller does not provide a timeout expiry.
      max: null
      metavar: null
      min: 5
      mutable: false
      name: default_notify_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 600
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_sender_link_timeout
      help: The duration to schedule a purge of idle sender links. Detach link after
        expiry.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: default_sender_link_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: dynamic
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: addressing_mode
      help: 'Indicates the addressing mode used by the driver.

        Permitted values:

        ''legacy''   - use legacy non-routable addressing

        ''routable'' - use routable addresses

        ''dynamic''  - use legacy addresses if the message bus does not support routing
        otherwise use routable addressing'
      max: null
      metavar: null
      min: null
      mutable: false
      name: addressing_mode
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pseudo_vhost
      help: Enable virtual host support for those message buses that do not natively
        support virtual hosting (such as qpidd). When set to true the virtual host
        name will be added to all message bus addresses, effectively creating a private
        'subnet' per virtual host. Set to False if the message bus supports virtual
        hosting using the 'hostname' field in the AMQP 1.0 Open performative as the
        name of the virtual host.
      max: null
      metavar: null
      min: null
      mutable: false
      name: pseudo_vhost
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: exclusive
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: server_request_prefix
      deprecated_reason: null
      deprecated_since: null
      dest: server_request_prefix
      help: address prefix used when sending to a specific server
      max: null
      metavar: null
      min: null
      mutable: false
      name: server_request_prefix
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: broadcast
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: broadcast_prefix
      deprecated_reason: null
      deprecated_since: null
      dest: broadcast_prefix
      help: address prefix used when broadcasting to all servers
      max: null
      metavar: null
      min: null
      mutable: false
      name: broadcast_prefix
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: unicast
      deprecated_for_removal: false
      deprecated_opts:
      - group: amqp1
        name: group_request_prefix
      deprecated_reason: null
      deprecated_since: null
      dest: group_request_prefix
      help: address prefix when sending to any server in group
      max: null
      metavar: null
      min: null
      mutable: false
      name: group_request_prefix
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: openstack.org/om/rpc
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rpc_address_prefix
      help: Address prefix for all generated RPC addresses
      max: null
      metavar: null
      min: null
      mutable: false
      name: rpc_address_prefix
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: openstack.org/om/notify
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: notify_address_prefix
      help: Address prefix for all generated Notification addresses
      max: null
      metavar: null
      min: null
      mutable: false
      name: notify_address_prefix
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: multicast
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: multicast_address
      help: Appended to the address prefix when sending a fanout message. Used by
        the message bus to identify fanout messages.
      max: null
      metavar: null
      min: null
      mutable: false
      name: multicast_address
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: unicast
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: unicast_address
      help: Appended to the address prefix when sending to a particular RPC/Notification
        server. Used by the message bus to identify messages sent to a single destination.
      max: null
      metavar: null
      min: null
      mutable: false
      name: unicast_address
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: anycast
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: anycast_address
      help: Appended to the address prefix when sending to a group of consumers. Used
        by the message bus to identify messages that should be delivered in a round-robin
        fashion across consumers.
      max: null
      metavar: null
      min: null
      mutable: false
      name: anycast_address
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_notification_exchange
      help: 'Exchange name used in notification addresses.

        Exchange name resolution precedence:

        Target.exchange if set

        else default_notification_exchange if set

        else control_exchange if set

        else ''notify'''
      max: null
      metavar: null
      min: null
      mutable: false
      name: default_notification_exchange
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: default_rpc_exchange
      help: 'Exchange name used in RPC addresses.

        Exchange name resolution precedence:

        Target.exchange if set

        else default_rpc_exchange if set

        else control_exchange if set

        else ''rpc'''
      max: null
      metavar: null
      min: null
      mutable: false
      name: default_rpc_exchange
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 200
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: reply_link_credit
      help: Window size for incoming RPC Reply messages.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: reply_link_credit
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 100
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rpc_server_credit
      help: Window size for incoming RPC Request messages
      max: null
      metavar: null
      min: 1
      mutable: false
      name: rpc_server_credit
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 100
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: notify_server_credit
      help: Window size for incoming Notification messages
      max: null
      metavar: null
      min: 1
      mutable: false
      name: notify_server_credit
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default:
      - rpc-cast
      - rpc-reply
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: pre_settled
      help: 'Send messages of this type pre-settled.

        Pre-settled messages will not receive acknowledgement

        from the peer. Note well: pre-settled messages may be

        silently discarded if the delivery fails.

        Permitted values:

        ''rpc-call'' - send RPC Calls pre-settled

        ''rpc-reply''- send RPC Replies pre-settled

        ''rpc-cast'' - Send RPC Casts pre-settled

        ''notify''   - Send Notifications pre-settled

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: pre_settled
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: multi valued
    standard_opts:
    - container_name
    - idle_timeout
    - trace
    - ssl
    - ssl_ca_file
    - ssl_cert_file
    - ssl_key_file
    - ssl_key_password
    - ssl_verify_vhost
    - sasl_mechanisms
    - sasl_config_dir
    - sasl_config_name
    - sasl_default_realm
    - connection_retry_interval
    - connection_retry_backoff
    - connection_retry_interval_max
    - link_retry_delay
    - default_reply_retry
    - default_reply_timeout
    - default_send_timeout
    - default_notify_timeout
    - default_sender_link_timeout
    - addressing_mode
    - pseudo_vhost
    - server_request_prefix
    - broadcast_prefix
    - group_request_prefix
    - rpc_address_prefix
    - notify_address_prefix
    - multicast_address
    - unicast_address
    - anycast_address
    - default_notification_exchange
    - default_rpc_exchange
    - reply_link_credit
    - rpc_server_credit
    - notify_server_credit
    - pre_settled
  oslo_messaging_kafka:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 1048576
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: kafka_max_fetch_bytes
      help: Max fetch bytes of Kafka consumer
      max: null
      metavar: null
      min: null
      mutable: false
      name: kafka_max_fetch_bytes
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 1.0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: kafka_consumer_timeout
      help: Default timeout(s) for Kafka consumers
      max: null
      metavar: null
      min: null
      mutable: false
      name: kafka_consumer_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: floating point value
    - advanced: false
      choices: []
      default: 10
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: 'Driver no longer uses connection pool. '
      deprecated_since: null
      dest: pool_size
      help: Pool Size for Kafka Consumers
      max: null
      metavar: null
      min: null
      mutable: false
      name: pool_size
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 2
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: 'Driver no longer uses connection pool. '
      deprecated_since: null
      dest: conn_pool_min_size
      help: The pool size limit for connections expiration policy
      max: null
      metavar: null
      min: null
      mutable: false
      name: conn_pool_min_size
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 1200
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: 'Driver no longer uses connection pool. '
      deprecated_since: null
      dest: conn_pool_ttl
      help: The time-to-live in sec of idle connections in the pool
      max: null
      metavar: null
      min: null
      mutable: false
      name: conn_pool_ttl
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: oslo_messaging_consumer
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: consumer_group
      help: Group id for Kafka consumer. Consumers in one group will coordinate message
        consumption
      max: null
      metavar: null
      min: null
      mutable: false
      name: consumer_group
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 0.0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: producer_batch_timeout
      help: Upper bound on the delay for KafkaProducer batching in seconds
      max: null
      metavar: null
      min: null
      mutable: false
      name: producer_batch_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: floating point value
    - advanced: false
      choices: []
      default: 16384
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: producer_batch_size
      help: Size of batch for the producer async send
      max: null
      metavar: null
      min: null
      mutable: false
      name: producer_batch_size
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - none
        - null
      - - gzip
        - null
      - - snappy
        - null
      - - lz4
        - null
      - - zstd
        - null
      default: none
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: compression_codec
      help: The compression codec for all data generated by the producer. If not set,
        compression will not be used. Note that the allowed values of this depend
        on the kafka version
      max: null
      metavar: null
      min: null
      mutable: false
      name: compression_codec
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: enable_auto_commit
      help: Enable asynchronous consumer commits
      max: null
      metavar: null
      min: null
      mutable: false
      name: enable_auto_commit
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 500
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_poll_records
      help: The maximum number of records returned in a poll call
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_poll_records
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - PLAINTEXT
        - null
      - - SASL_PLAINTEXT
        - null
      - - SSL
        - null
      - - SASL_SSL
        - null
      default: PLAINTEXT
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: security_protocol
      help: Protocol used to communicate with brokers
      max: null
      metavar: null
      min: null
      mutable: false
      name: security_protocol
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: PLAIN
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: sasl_mechanism
      help: Mechanism when security protocol is SASL
      max: null
      metavar: null
      min: null
      mutable: false
      name: sasl_mechanism
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_cafile
      help: CA certificate PEM file used to verify the server certificate
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_cafile
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_client_cert_file
      help: Client certificate PEM file used for authentication.
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_client_cert_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_client_key_file
      help: Client key PEM file used for authentication.
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_client_key_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_client_key_password
      help: Client key password file used for authentication.
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_client_key_password
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - kafka_max_fetch_bytes
    - kafka_consumer_timeout
    - pool_size
    - conn_pool_min_size
    - conn_pool_ttl
    - consumer_group
    - producer_batch_timeout
    - producer_batch_size
    - compression_codec
    - enable_auto_commit
    - max_poll_records
    - security_protocol
    - sasl_mechanism
    - ssl_cafile
    - ssl_client_cert_file
    - ssl_client_key_file
    - ssl_client_key_password
  oslo_messaging_notifications:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: notification_driver
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: The Drivers(s) to handle sending notifications. Possible values are messaging,
        messagingv2, routing, log, test, noop
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: multi valued
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: notification_transport_url
      deprecated_reason: null
      deprecated_since: null
      dest: transport_url
      help: A URL representing the messaging driver to use for notifications. If not
        set, we fall back to the same configuration used for RPC.
      max: null
      metavar: null
      min: null
      mutable: false
      name: transport_url
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: true
      short: null
      type: string value
    - advanced: false
      choices: []
      default:
      - notifications
      deprecated_for_removal: false
      deprecated_opts:
      - group: rpc_notifier2
        name: topics
      - group: DEFAULT
        name: notification_topics
      deprecated_reason: null
      deprecated_since: null
      dest: topics
      help: AMQP topic used for OpenStack notifications.
      max: null
      metavar: null
      min: null
      mutable: false
      name: topics
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: list value
    - advanced: false
      choices: []
      default: -1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: retry
      help: The maximum number of attempts to re-send a notification message which
        failed to be delivered due to a recoverable error. 0 - No retry, -1 - indefinite
      max: null
      metavar: null
      min: null
      mutable: false
      name: retry
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - transport_url
    - topics
    - retry
  oslo_messaging_rabbit:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: amqp_durable_queues
      help: Use durable queues in AMQP.
      max: null
      metavar: null
      min: null
      mutable: false
      name: amqp_durable_queues
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: amqp_auto_delete
      deprecated_reason: null
      deprecated_since: null
      dest: amqp_auto_delete
      help: Auto-delete queues in AMQP.
      max: null
      metavar: null
      min: null
      mutable: false
      name: amqp_auto_delete
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts:
      - group: oslo_messaging_rabbit
        name: rabbit_use_ssl
      deprecated_reason: null
      deprecated_since: null
      dest: ssl
      help: Connect over SSL.
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: oslo_messaging_rabbit
        name: kombu_ssl_version
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_version
      help: SSL version to use (valid only if SSL enabled). Valid values are TLSv1
        and SSLv23. SSLv2, SSLv3, TLSv1_1, and TLSv1_2 may be available on some distributions.
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_version
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: oslo_messaging_rabbit
        name: kombu_ssl_keyfile
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_key_file
      help: SSL key file (valid only if SSL enabled).
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_key_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: oslo_messaging_rabbit
        name: kombu_ssl_certfile
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_cert_file
      help: SSL cert file (valid only if SSL enabled).
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_cert_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: ''
      deprecated_for_removal: false
      deprecated_opts:
      - group: oslo_messaging_rabbit
        name: kombu_ssl_ca_certs
      deprecated_reason: null
      deprecated_since: null
      dest: ssl_ca_file
      help: SSL certification authority file (valid only if SSL enabled).
      max: null
      metavar: null
      min: null
      mutable: false
      name: ssl_ca_file
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: heartbeat_in_pthread
      help: 'EXPERIMENTAL: Run the health check heartbeat thread through a native
        python thread. By default if this option isn''t provided the  health check
        heartbeat will inherit the execution model from the parent process. By example
        if the parent process have monkey patched the stdlib by using eventlet/greenlet
        then the heartbeat will be run through a green thread.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: heartbeat_in_pthread
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 1.0
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: kombu_reconnect_delay
      deprecated_reason: null
      deprecated_since: null
      dest: kombu_reconnect_delay
      help: How long to wait before reconnecting in response to an AMQP consumer cancel
        notification.
      max: null
      metavar: null
      min: null
      mutable: false
      name: kombu_reconnect_delay
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: floating point value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: kombu_compression
      help: 'EXPERIMENTAL: Possible values are: gzip, bz2. If not set compression
        will not be used. This option may not be available in future versions.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: kombu_compression
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 60
      deprecated_for_removal: false
      deprecated_opts:
      - group: oslo_messaging_rabbit
        name: kombu_reconnect_timeout
      deprecated_reason: null
      deprecated_since: null
      dest: kombu_missing_consumer_retry_timeout
      help: How long to wait a missing client before abandoning to send it its replies.
        This value should not be longer than rpc_response_timeout.
      max: null
      metavar: null
      min: null
      mutable: false
      name: kombu_missing_consumer_retry_timeout
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - round-robin
        - null
      - - shuffle
        - null
      default: round-robin
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: kombu_failover_strategy
      help: Determines how the next RabbitMQ node is chosen in case the one we are
        currently connected to becomes unavailable. Takes effect only if more than
        one RabbitMQ node is provided in config.
      max: null
      metavar: null
      min: null
      mutable: false
      name: kombu_failover_strategy
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices:
      - - PLAIN
        - null
      - - AMQPLAIN
        - null
      - - RABBIT-CR-DEMO
        - null
      default: AMQPLAIN
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: rabbit_login_method
      deprecated_reason: null
      deprecated_since: null
      dest: rabbit_login_method
      help: The RabbitMQ login method.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rabbit_login_method
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rabbit_retry_interval
      help: How frequently to retry connecting with RabbitMQ.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rabbit_retry_interval
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 2
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: rabbit_retry_backoff
      deprecated_reason: null
      deprecated_since: null
      dest: rabbit_retry_backoff
      help: How long to backoff for between retries when connecting to RabbitMQ.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rabbit_retry_backoff
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 30
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rabbit_interval_max
      help: Maximum interval of RabbitMQ connection retries. Default is 30 seconds.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rabbit_interval_max
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: rabbit_ha_queues
      deprecated_reason: null
      deprecated_since: null
      dest: rabbit_ha_queues
      help: 'Try to use HA queues in RabbitMQ (x-ha-policy: all). If you change this
        option, you must wipe the RabbitMQ database. In RabbitMQ 3.0, queue mirroring
        is no longer controlled by the x-ha-policy argument when declaring a queue.
        If you just want to make sure that all queues (except those with auto-generated
        names) are mirrored across all nodes, run: "rabbitmqctl set_policy HA ''^(?!amq\.).*''
        ''{"ha-mode": "all"}'' "'
      max: null
      metavar: null
      min: null
      mutable: false
      name: rabbit_ha_queues
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 1800
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rabbit_transient_queues_ttl
      help: Positive integer representing duration in seconds for queue TTL (x-expires).
        Queues which are unused for the duration of the TTL are automatically deleted.
        The parameter affects only reply and fanout queues.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: rabbit_transient_queues_ttl
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: rabbit_qos_prefetch_count
      help: Specifies the number of messages to prefetch. Setting to zero allows unlimited
        messages.
      max: null
      metavar: null
      min: null
      mutable: false
      name: rabbit_qos_prefetch_count
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 60
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: heartbeat_timeout_threshold
      help: Number of seconds after which the Rabbit broker is considered down if
        heartbeat's keep-alive fails (0 disables heartbeat).
      max: null
      metavar: null
      min: null
      mutable: false
      name: heartbeat_timeout_threshold
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 2
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: heartbeat_rate
      help: How often times during the heartbeat_timeout_threshold we check the heartbeat.
      max: null
      metavar: null
      min: null
      mutable: false
      name: heartbeat_rate
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: direct_mandatory_flag
      help: Enable/Disable the RabbitMQ mandatory flag for direct send. The direct
        send is used as reply, so the MessageUndeliverable exception is raised in
        case the client queue does not exist.
      max: null
      metavar: null
      min: null
      mutable: false
      name: direct_mandatory_flag
      namespace: oslo.messaging
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - amqp_durable_queues
    - amqp_auto_delete
    - ssl
    - ssl_version
    - ssl_key_file
    - ssl_cert_file
    - ssl_ca_file
    - heartbeat_in_pthread
    - kombu_reconnect_delay
    - kombu_compression
    - kombu_missing_consumer_retry_timeout
    - kombu_failover_strategy
    - rabbit_login_method
    - rabbit_retry_interval
    - rabbit_retry_backoff
    - rabbit_interval_max
    - rabbit_ha_queues
    - rabbit_transient_queues_ttl
    - rabbit_qos_prefetch_count
    - heartbeat_timeout_threshold
    - heartbeat_rate
    - direct_mandatory_flag
  oslo_middleware:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 114688
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: osapi_max_request_body_size
      - group: DEFAULT
        name: max_request_body_size
      deprecated_reason: null
      deprecated_since: null
      dest: max_request_body_size
      help: The maximum body size for each  request, in bytes.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_request_body_size
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: X-Forwarded-Proto
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: secure_proxy_ssl_header
      help: The HTTP Header that will be used to determine what the original request
        protocol scheme was, even if it was hidden by a SSL termination proxy.
      max: null
      metavar: null
      min: null
      mutable: false
      name: secure_proxy_ssl_header
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: enable_proxy_headers_parsing
      help: Whether the application is behind a proxy or not. This determines if the
        middleware should parse the headers or not.
      max: null
      metavar: null
      min: null
      mutable: false
      name: enable_proxy_headers_parsing
      namespace: oslo.middleware
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - max_request_body_size
    - secure_proxy_ssl_header
    - enable_proxy_headers_parsing
  oslo_policy:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: enforce_scope
      help: This option controls whether or not to enforce scope when evaluating policies.
        If ``True``, the scope of the token used in the request is compared to the
        ``scope_types`` of the policy being enforced. If the scopes do not match,
        an ``InvalidScope`` exception will be raised. If ``False``, a message will
        be logged informing operators that policies are being invoked with mismatching
        scope.
      max: null
      metavar: null
      min: null
      mutable: false
      name: enforce_scope
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: policy.json
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: policy_file
      deprecated_reason: null
      deprecated_since: null
      dest: policy_file
      help: The relative or absolute path of a file that maps roles to permissions
        for a given service. Relative paths must be specified in relation to the configuration
        file setting this option.
      max: null
      metavar: null
      min: null
      mutable: false
      name: policy_file
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: default
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: policy_default_rule
      deprecated_reason: null
      deprecated_since: null
      dest: policy_default_rule
      help: Default rule. Enforced when a requested rule is not found.
      max: null
      metavar: null
      min: null
      mutable: false
      name: policy_default_rule
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default:
      - policy.d
      deprecated_for_removal: false
      deprecated_opts:
      - group: DEFAULT
        name: policy_dirs
      deprecated_reason: null
      deprecated_since: null
      dest: policy_dirs
      help: Directories where policy configuration files are stored. They can be relative
        to any directory in the search path defined by the config_dir option, or absolute
        paths. The file defined by policy_file must exist for these directories to
        be searched.  Missing or empty directories are ignored.
      max: null
      metavar: null
      min: null
      mutable: false
      name: policy_dirs
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: multi valued
    - advanced: false
      choices:
      - - application/x-www-form-urlencoded
        - null
      - - application/json
        - null
      default: application/x-www-form-urlencoded
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: remote_content_type
      help: Content Type to send and receive data for REST based policy check
      max: null
      metavar: null
      min: null
      mutable: false
      name: remote_content_type
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: remote_ssl_verify_server_crt
      help: server identity verification for REST based policy check
      max: null
      metavar: null
      min: null
      mutable: false
      name: remote_ssl_verify_server_crt
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: remote_ssl_ca_crt_file
      help: Absolute path to ca cert file for REST based policy check
      max: null
      metavar: null
      min: null
      mutable: false
      name: remote_ssl_ca_crt_file
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: remote_ssl_client_crt_file
      help: Absolute path to client cert for REST based policy check
      max: null
      metavar: null
      min: null
      mutable: false
      name: remote_ssl_client_crt_file
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: remote_ssl_client_key_file
      help: Absolute path client key file REST based policy check
      max: null
      metavar: null
      min: null
      mutable: false
      name: remote_ssl_client_key_file
      namespace: oslo.policy
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - enforce_scope
    - policy_file
    - policy_default_rule
    - policy_dirs
    - remote_content_type
    - remote_ssl_verify_server_crt
    - remote_ssl_ca_crt_file
    - remote_ssl_client_crt_file
    - remote_ssl_client_key_file
  policy:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the policy backend driver in the `keystone.policy` namespace.
        Supplied drivers are `rules` (which does not support any CRUD operations for
        the v3 policy API) and `sql`. Typically, there is no reason to set this option
        unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: list_limit
      help: Maximum number of entities that will be returned in a policy collection.
      max: null
      metavar: null
      min: null
      mutable: false
      name: list_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - list_limit
  profiler:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts:
      - group: profiler
        name: profiler_enabled
      deprecated_reason: null
      deprecated_since: null
      dest: enabled
      help: "\nEnable the profiling for all services on this node.\n\nDefault value\
        \ is False (fully disable the profiling feature).\n\nPossible values:\n\n\
        * True: Enables the feature\n* False: Disables the feature. The profiling\
        \ cannot be started via this project\n  operations. If the profiling is triggered\
        \ by another project, this project\n  part will be empty.\n"
      max: null
      metavar: null
      min: null
      mutable: false
      name: enabled
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: trace_sqlalchemy
      help: "\nEnable SQL requests profiling in services.\n\nDefault value is False\
        \ (SQL requests won't be traced).\n\nPossible values:\n\n* True: Enables SQL\
        \ requests profiling. Each SQL query will be part of the\n  trace and can\
        \ the be analyzed by how much time was spent for that.\n* False: Disables\
        \ SQL requests profiling. The spent time is only shown on a\n  higher level\
        \ of operations. Single SQL queries cannot be analyzed this way.\n"
      max: null
      metavar: null
      min: null
      mutable: false
      name: trace_sqlalchemy
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: SECRET_KEY
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: hmac_keys
      help: '

        Secret key(s) to use for encrypting context data for performance profiling.


        This string value should have the following format: <key1>[,<key2>,...<keyn>],

        where each key is some random string. A user who triggers the profiling via

        the REST API has to set one of these keys in the headers of the REST API call

        to include profiling results of this node for this particular project.


        Both "enabled" flag and "hmac_keys" config options should be set to enable

        profiling. Also, to generate correct profiling information across all services

        at least one key needs to be consistent between OpenStack projects. This

        ensures it can be used from client side to generate the trace, containing

        information from all possible resources.

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: hmac_keys
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: messaging://
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: connection_string
      help: "\nConnection string for a notifier backend.\n\nDefault value is ``messaging://``\
        \ which sets the notifier to oslo_messaging.\n\nExamples of possible values:\n\
        \n* ``messaging://`` - use oslo_messaging driver for sending spans.\n* ``redis://127.0.0.1:6379``\
        \ - use redis driver for sending spans.\n* ``mongodb://127.0.0.1:27017`` -\
        \ use mongodb driver for sending spans.\n* ``elasticsearch://127.0.0.1:9200``\
        \ - use elasticsearch driver for sending\n  spans.\n* ``jaeger://127.0.0.1:6831``\
        \ - use jaeger tracing as driver for sending spans.\n"
      max: null
      metavar: null
      min: null
      mutable: false
      name: connection_string
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: notification
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: es_doc_type
      help: '

        Document type for notification indexing in elasticsearch.

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: es_doc_type
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 2m
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: es_scroll_time
      help: '

        This parameter is a time value parameter (for example: es_scroll_time=2m),

        indicating for how long the nodes that participate in the search will maintain

        relevant resources in order to continue and support it.

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: es_scroll_time
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 10000
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: es_scroll_size
      help: '

        Elasticsearch splits large requests in batches. This parameter defines

        maximum size of each batch (for example: es_scroll_size=10000).

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: es_scroll_size
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0.1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: socket_timeout
      help: '

        Redissentinel provides a timeout option on the connections.

        This parameter defines that timeout (for example: socket_timeout=0.1).

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: socket_timeout
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: floating point value
    - advanced: false
      choices: []
      default: mymaster
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: sentinel_service_name
      help: '

        Redissentinel uses a service name to identify a master redis service.

        This parameter defines the name (for example:

        ``sentinal_service_name=mymaster``).

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: sentinel_service_name
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: filter_error_trace
      help: '

        Enable filter traces that contain error/exception to a separated place.


        Default value is set to False.


        Possible values:


        * True: Enable filter traces that contain error/exception.

        * False: Disable the filter.

        '
      max: null
      metavar: null
      min: null
      mutable: false
      name: filter_error_trace
      namespace: osprofiler
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - enabled
    - trace_sqlalchemy
    - hmac_keys
    - connection_string
    - es_doc_type
    - es_scroll_time
    - es_scroll_size
    - socket_timeout
    - sentinel_service_name
    - filter_error_trace
  receipt:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 300
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: expiration
      help: The amount of time that a receipt should remain valid (in seconds). This
        value should always be very short, as it represents how long a user has to
        reattempt auth with the missing auth methods.
      max: 86400
      metavar: null
      min: 0
      mutable: false
      name: expiration
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: fernet
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: provider
      help: Entry point for the receipt provider in the `keystone.receipt.provider`
        namespace. The receipt provider controls the receipt construction and validation
        operations. Keystone includes just the `fernet` receipt provider for now.
        `fernet` receipts do not need to be persisted at all, but require that you
        run `keystone-manage fernet_setup` (also see the `keystone-manage fernet_rotate`
        command).
      max: null
      metavar: null
      min: null
      mutable: false
      name: provider
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for caching receipt creation and validation data. This has no effect
        unless global caching is enabled, or if cache_on_issue is disabled as we only
        cache receipts on issue.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 300
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: The number of seconds to cache receipt creation and validation data. This
        has no effect unless both global and `[receipt] caching` are enabled.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_on_issue
      help: Enable storing issued receipt data to receipt validation cache so that
        first receipt validation doesn't actually cause full validation cycle. This
        option has no effect unless global caching and receipt caching are enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_on_issue
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - expiration
    - provider
    - caching
    - cache_time
    - cache_on_issue
  resource:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the resource driver in the `keystone.resource` namespace.
        Only a `sql` driver is supplied by keystone. Unless you are writing proprietary
        drivers for keystone, you do not need to set this option.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts:
      - group: assignment
        name: caching
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for resource caching. This has no effect unless global caching
        is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: assignment
        name: cache_time
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache resource data in seconds. This has no effect unless global
        caching is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts:
      - group: assignment
        name: list_limit
      deprecated_reason: null
      deprecated_since: null
      dest: list_limit
      help: Maximum number of entities that will be returned in a resource collection.
      max: null
      metavar: null
      min: null
      mutable: false
      name: list_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: admin_project_domain_name
      help: Name of the domain that owns the `admin_project_name`. If left unset,
        then there is no admin project. `[resource] admin_project_name` must also
        be set to use this option.
      max: null
      metavar: null
      min: null
      mutable: false
      name: admin_project_domain_name
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: admin_project_name
      help: This is a special project which represents cloud-level administrator privileges
        across services. Tokens scoped to this project will contain a true `is_admin_project`
        attribute to indicate to policy systems that the role assignments on that
        specific project should apply equally across every project. If left unset,
        then there is no admin project, and thus no explicit means of cross-project
        role assignments. `[resource] admin_project_domain_name` must also be set
        to use this option.
      max: null
      metavar: null
      min: null
      mutable: false
      name: admin_project_name
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices:
      - - 'off'
        - null
      - - new
        - null
      - - strict
        - null
      default: 'off'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: project_name_url_safe
      help: This controls whether the names of projects are restricted from containing
        URL-reserved characters. If set to `new`, attempts to create or update a project
        with a URL-unsafe name will fail. If set to `strict`, attempts to scope a
        token with a URL-unsafe project name will fail, thereby forcing all project
        names to be updated to be URL-safe.
      max: null
      metavar: null
      min: null
      mutable: false
      name: project_name_url_safe
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices:
      - - 'off'
        - null
      - - new
        - null
      - - strict
        - null
      default: 'off'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: domain_name_url_safe
      help: This controls whether the names of domains are restricted from containing
        URL-reserved characters. If set to `new`, attempts to create or update a domain
        with a URL-unsafe name will fail. If set to `strict`, attempts to scope a
        token with a URL-unsafe domain name will fail, thereby forcing all domain
        names to be updated to be URL-safe.
      max: null
      metavar: null
      min: null
      mutable: false
      name: domain_name_url_safe
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - driver
    - caching
    - cache_time
    - list_limit
    - admin_project_domain_name
    - admin_project_name
    - project_name_url_safe
    - domain_name_url_safe
  revoke:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the token revocation backend driver in the `keystone.revoke`
        namespace. Keystone only provides a `sql` driver, so there is no reason to
        set this option unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 1800
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: expiration_buffer
      help: The number of seconds after a token has expired before a corresponding
        revocation event may be purged from the backend.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: expiration_buffer
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for revocation event caching. This has no effect unless global
        caching is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 3600
      deprecated_for_removal: false
      deprecated_opts:
      - group: token
        name: revocation_cache_time
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache the revocation list and the revocation events (in seconds).
        This has no effect unless global and `[revoke] caching` are both enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - expiration_buffer
    - caching
    - cache_time
  role:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the role backend driver in the `keystone.role` namespace.
        Keystone only provides a `sql` driver, so there's no reason to change this
        unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for role caching. This has no effect unless global caching is enabled.
        In a typical deployment, there is no reason to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache role data, in seconds. This has no effect unless both global
        caching and `[role] caching` are enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: list_limit
      help: Maximum number of entities that will be returned in a role collection.
        This may be useful to tune if you have a large number of discrete roles in
        your deployment.
      max: null
      metavar: null
      min: null
      mutable: false
      name: list_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - driver
    - caching
    - cache_time
    - list_limit
  saml:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 3600
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: assertion_expiration_time
      help: Determines the lifetime for any SAML assertions generated by keystone,
        using `NotOnOrAfter` attributes.
      max: null
      metavar: null
      min: null
      mutable: false
      name: assertion_expiration_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: xmlsec1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: xmlsec1_binary
      help: Name of, or absolute path to, the binary to be used for XML signing. Although
        only the XML Security Library (`xmlsec1`) is supported, it may have a non-standard
        name or path on your system. If keystone cannot find the binary itself, you
        may need to install the appropriate package, use this option to specify an
        absolute path, or adjust keystone's PATH environment variable.
      max: null
      metavar: null
      min: null
      mutable: false
      name: xmlsec1_binary
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: /etc/keystone/ssl/certs/signing_cert.pem
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: certfile
      help: Absolute path to the public certificate file to use for SAML signing.
        The value cannot contain a comma (`,`).
      max: null
      metavar: null
      min: null
      mutable: false
      name: certfile
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: /etc/keystone/ssl/private/signing_key.pem
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: keyfile
      help: Absolute path to the private key file to use for SAML signing. The value
        cannot contain a comma (`,`).
      max: null
      metavar: null
      min: null
      mutable: false
      name: keyfile
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_entity_id
      help: 'This is the unique entity identifier of the identity provider (keystone)
        to use when generating SAML assertions. This value is required to generate
        identity provider metadata and must be a URI (a URL is recommended). For example:
        `https://keystone.example.com/v3/OS-FEDERATION/saml2/idp`.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_entity_id
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: uri value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_sso_endpoint
      help: 'This is the single sign-on (SSO) service location of the identity provider
        which accepts HTTP POST requests. A value is required to generate identity
        provider metadata. For example: `https://keystone.example.com/v3/OS-FEDERATION/saml2/sso`.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_sso_endpoint
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: uri value
    - advanced: false
      choices: []
      default: en
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_lang
      help: This is the language used by the identity provider's organization.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_lang
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: SAML Identity Provider
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_organization_name
      help: This is the name of the identity provider's organization.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_organization_name
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: OpenStack SAML Identity Provider
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_organization_display_name
      help: This is the name of the identity provider's organization to be displayed.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_organization_display_name
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: https://example.com/
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_organization_url
      help: This is the URL of the identity provider's organization. The URL referenced
        here should be useful to humans.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_organization_url
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: uri value
    - advanced: false
      choices: []
      default: Example, Inc.
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_contact_company
      help: This is the company name of the identity provider's contact person.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_contact_company
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: SAML Identity Provider Support
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_contact_name
      help: This is the given name of the identity provider's contact person.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_contact_name
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: Support
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_contact_surname
      help: This is the surname of the identity provider's contact person.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_contact_surname
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: support@example.com
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_contact_email
      help: This is the email address of the identity provider's contact person.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_contact_email
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: +1 800 555 0100
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_contact_telephone
      help: This is the telephone number of the identity provider's contact person.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_contact_telephone
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices:
      - - technical
        - null
      - - support
        - null
      - - administrative
        - null
      - - billing
        - null
      - - other
        - null
      default: other
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_contact_type
      help: This is the type of contact that best describes the identity provider's
        contact person.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_contact_type
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: /etc/keystone/saml2_idp_metadata.xml
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: idp_metadata_path
      help: Absolute path to the identity provider metadata file. This file should
        be generated with the `keystone-manage saml_idp_metadata` command. There is
        typically no reason to change this value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: idp_metadata_path
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: 'ss:mem:'
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: relay_state_prefix
      help: The prefix of the RelayState SAML attribute to use when generating enhanced
        client and proxy (ECP) assertions. In a typical deployment, there is no reason
        to change this value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: relay_state_prefix
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - assertion_expiration_time
    - xmlsec1_binary
    - certfile
    - keyfile
    - idp_entity_id
    - idp_sso_endpoint
    - idp_lang
    - idp_organization_name
    - idp_organization_display_name
    - idp_organization_url
    - idp_contact_company
    - idp_contact_name
    - idp_contact_surname
    - idp_contact_email
    - idp_contact_telephone
    - idp_contact_type
    - idp_metadata_path
    - relay_state_prefix
  security_compliance:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: disable_user_account_days_inactive
      help: The maximum number of days a user can go without authenticating before
        being considered "inactive" and automatically disabled (locked). This feature
        is disabled by default; set any value to enable it. This feature depends on
        the `sql` backend for the `[identity] driver`. When a user exceeds this threshold
        and is considered "inactive", the user's `enabled` attribute in the HTTP API
        may not match the value of the user's `enabled` column in the user table.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: disable_user_account_days_inactive
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: lockout_failure_attempts
      help: The maximum number of times that a user can fail to authenticate before
        the user account is locked for the number of seconds specified by `[security_compliance]
        lockout_duration`. This feature is disabled by default. If this feature is
        enabled and `[security_compliance] lockout_duration` is not set, then users
        may be locked out indefinitely until the user is explicitly enabled via the
        API. This feature depends on the `sql` backend for the `[identity] driver`.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: lockout_failure_attempts
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 1800
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: lockout_duration
      help: The number of seconds a user account will be locked when the maximum number
        of failed authentication attempts (as specified by `[security_compliance]
        lockout_failure_attempts`) is exceeded. Setting this option will have no effect
        unless you also set `[security_compliance] lockout_failure_attempts` to a
        non-zero value. This feature depends on the `sql` backend for the `[identity]
        driver`.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: lockout_duration
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: password_expires_days
      help: The number of days for which a password will be considered valid before
        requiring it to be changed. This feature is disabled by default. If enabled,
        new password changes will have an expiration date, however existing passwords
        would not be impacted. This feature depends on the `sql` backend for the `[identity]
        driver`.
      max: null
      metavar: null
      min: 1
      mutable: false
      name: password_expires_days
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: unique_last_password_count
      help: This controls the number of previous user password iterations to keep
        in history, in order to enforce that newly created passwords are unique. The
        total number which includes the new password should not be greater or equal
        to this value. Setting the value to zero (the default) disables this feature.
        Thus, to enable this feature, values must be greater than 0. This feature
        depends on the `sql` backend for the `[identity] driver`.
      max: null
      metavar: null
      min: 0
      mutable: false
      name: unique_last_password_count
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: 0
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: minimum_password_age
      help: 'The number of days that a password must be used before the user can change
        it. This prevents users from changing their passwords immediately in order
        to wipe out their password history and reuse an old password. This feature
        does not prevent administrators from manually resetting passwords. It is disabled
        by default and allows for immediate password changes. This feature depends
        on the `sql` backend for the `[identity] driver`. Note: If `[security_compliance]
        password_expires_days` is set, then the value for this option should be less
        than the `password_expires_days`.'
      max: null
      metavar: null
      min: 0
      mutable: false
      name: minimum_password_age
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: password_regex
      help: 'The regular expression used to validate password strength requirements.
        By default, the regular expression will match any password. The following
        is an example of a pattern which requires at least 1 letter, 1 digit, and
        have a minimum length of 7 characters: ^(?=.*\d)(?=.*[a-zA-Z]).{7,}$ This
        feature depends on the `sql` backend for the `[identity] driver`.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: password_regex
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: password_regex_description
      help: Describe your password regular expression here in language for humans.
        If a password fails to match the regular expression, the contents of this
        configuration variable will be returned to users to explain why their requested
        password was insufficient.
      max: null
      metavar: null
      min: null
      mutable: false
      name: password_regex_description
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: change_password_upon_first_use
      help: Enabling this option requires users to change their password when the
        user is created, or upon administrative reset. Before accessing any services,
        affected users will have to change their password. To ignore this requirement
        for specific users, such as service users, set the `options` attribute `ignore_change_password_upon_first_use`
        to `True` for the desired user via the update user API. This feature is disabled
        by default. This feature is only applicable with the `sql` backend for the
        `[identity] driver`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: change_password_upon_first_use
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - disable_user_account_days_inactive
    - lockout_failure_attempts
    - lockout_duration
    - password_expires_days
    - unique_last_password_count
    - minimum_password_age
    - password_regex
    - password_regex_description
    - change_password_upon_first_use
  shadow_users:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the shadow users backend driver in the `keystone.identity.shadow_users`
        namespace. This driver is used for persisting local user references to externally-managed
        identities (via federation, LDAP, etc). Keystone only provides a `sql` driver,
        so there is no reason to change this option unless you are providing a custom
        entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - driver
  token:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 3600
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: expiration
      help: The amount of time that a token should remain valid (in seconds). Drastically
        reducing this value may break "long-running" operations that involve multiple
        services to coordinate together, and will force users to authenticate with
        keystone more frequently. Drastically increasing this value will increase
        the number of tokens that will be simultaneously valid. Keystone tokens are
        also bearer tokens, so a shorter duration will also reduce the potential security
        impact of a compromised token.
      max: 9223372036854775807
      metavar: null
      min: 0
      mutable: false
      name: expiration
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: fernet
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: provider
      help: Entry point for the token provider in the `keystone.token.provider` namespace.
        The token provider controls the token construction, validation, and revocation
        operations. Supported upstream providers are `fernet` and `jws`. Neither `fernet`
        or `jws` tokens require persistence and both require additional setup. If
        using `fernet`, you're required to run `keystone-manage fernet_setup`, which
        creates symmetric keys used to encrypt tokens. If using `jws`, you're required
        to generate an ECDSA keypair using a SHA-256 hash algorithm for signing and
        validating token, which can be done with `keystone-manage create_jws_keypair`.
        Note that `fernet` tokens are encrypted and `jws` tokens are only signed.
        Please be sure to consider this if your deployment has security requirements
        regarding payload contents used to generate token IDs.
      max: null
      metavar: null
      min: null
      mutable: false
      name: provider
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for caching token creation and validation data. This has no effect
        unless global caching is enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: The number of seconds to cache token creation and validation data. This
        has no effect unless both global and `[token] caching` are enabled.
      max: 9223372036854775807
      metavar: null
      min: 0
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: revoke_by_id
      help: This toggles support for revoking individual tokens by the token identifier
        and thus various token enumeration operations (such as listing all tokens
        issued to a specific user). These operations are used to determine the list
        of tokens to consider revoked. Do not disable this option if you're using
        the `kvs` `[revoke] driver`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: revoke_by_id
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: allow_rescope_scoped_token
      help: This toggles whether scoped tokens may be re-scoped to a new project or
        domain, thereby preventing users from exchanging a scoped token (including
        those with a default project scope) for any other token. This forces users
        to either authenticate for unscoped tokens (and later exchange that unscoped
        token for tokens with a more specific scope) or to provide their credentials
        in every request for a scoped token to avoid re-scoping altogether.
      max: null
      metavar: null
      min: null
      mutable: false
      name: allow_rescope_scoped_token
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: true
      deprecated_opts: []
      deprecated_reason: Keystone already exposes a configuration option for caching
        tokens. Having a separate configuration option to cache tokens when they are
        issued is redundant, unnecessarily complicated, and is misleading if token
        caching is disabled because tokens will still be pre-cached by default when
        they are issued. The ability to pre-cache tokens when they are issued is going
        to rely exclusively on the ``keystone.conf [token] caching`` option in the
        future.
      deprecated_since: S
      dest: cache_on_issue
      help: Enable storing issued token data to token validation cache so that first
        token validation doesn't actually cause full validation cycle. This option
        has no effect unless global caching is enabled and will still cache tokens
        even if `[token] caching = False`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_on_issue
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 172800
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: allow_expired_window
      help: This controls the number of seconds that a token can be retrieved for
        beyond the built-in expiry time. This allows long running operations to succeed.
        Defaults to two days.
      max: null
      metavar: null
      min: null
      mutable: false
      name: allow_expired_window
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - expiration
    - provider
    - caching
    - cache_time
    - revoke_by_id
    - allow_rescope_scoped_token
    - cache_on_issue
    - allow_expired_window
  tokenless_auth:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: []
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: trusted_issuer
      help: The list of distinguished names which identify trusted issuers of client
        certificates allowed to use X.509 tokenless authorization. If the option is
        absent then no certificates will be allowed. The format for the values of
        a distinguished name (DN) must be separated by a comma and contain no spaces.
        Furthermore, because an individual DN may contain commas, this configuration
        option may be repeated multiple times to represent multiple values. For example,
        keystone.conf would include two consecutive lines in order to trust two different
        DNs, such as `trusted_issuer = CN=john,OU=keystone,O=openstack` and `trusted_issuer
        = CN=mary,OU=eng,O=abc`.
      max: null
      metavar: null
      min: null
      mutable: false
      name: trusted_issuer
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: multi valued
    - advanced: false
      choices: []
      default: x509
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: protocol
      help: The federated protocol ID used to represent X.509 tokenless authorization.
        This is used in combination with the value of `[tokenless_auth] issuer_attribute`
        to find a corresponding federated mapping. In a typical deployment, there
        is no reason to change this value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: protocol
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: SSL_CLIENT_I_DN
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: issuer_attribute
      help: The name of the WSGI environment variable used to pass the issuer of the
        client certificate to keystone. This attribute is used as an identity provider
        ID for the X.509 tokenless authorization along with the protocol to look up
        its corresponding mapping. In a typical deployment, there is no reason to
        change this value.
      max: null
      metavar: null
      min: null
      mutable: false
      name: issuer_attribute
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - trusted_issuer
    - protocol
    - issuer_attribute
  totp:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: 1
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: included_previous_windows
      help: The number of previous windows to check when processing TOTP passcodes.
      max: 10
      metavar: null
      min: 0
      mutable: false
      name: included_previous_windows
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    standard_opts:
    - included_previous_windows
  trust:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: allow_redelegation
      help: Allows authorization to be redelegated from one user to another, effectively
        chaining trusts together. When disabled, the `remaining_uses` attribute of
        a trust is constrained to be zero.
      max: null
      metavar: null
      min: null
      mutable: false
      name: allow_redelegation
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: 3
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: max_redelegation_count
      help: Maximum number of times that authorization can be redelegated from one
        user to another in a chain of trusts. This number may be reduced further for
        a specific trust.
      max: null
      metavar: null
      min: null
      mutable: false
      name: max_redelegation_count
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the trust backend driver in the `keystone.trust` namespace.
        Keystone only provides a `sql` driver, so there is no reason to change this
        unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - allow_redelegation
    - max_redelegation_count
    - driver
  unified_limit:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: sql
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: driver
      help: Entry point for the unified limit backend driver in the `keystone.unified_limit`
        namespace. Keystone only provides a `sql` driver, so there's no reason to
        change this unless you are providing a custom entry point.
      max: null
      metavar: null
      min: null
      mutable: false
      name: driver
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    - advanced: false
      choices: []
      default: true
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: caching
      help: Toggle for unified limit caching. This has no effect unless global caching
        is enabled. In a typical deployment, there is no reason to disable this.
      max: null
      metavar: null
      min: null
      mutable: false
      name: caching
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: cache_time
      help: Time to cache unified limit data, in seconds. This has no effect unless
        both global caching and `[unified_limit] caching` are enabled.
      max: null
      metavar: null
      min: null
      mutable: false
      name: cache_time
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices: []
      default: null
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: list_limit
      help: Maximum number of entities that will be returned in a role collection.
        This may be useful to tune if you have a large number of unified limits in
        your deployment.
      max: null
      metavar: null
      min: null
      mutable: false
      name: list_limit
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: integer value
    - advanced: false
      choices:
      - - flat
        - null
      - - strict_two_level
        - null
      default: flat
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: enforcement_model
      help: The enforcement model to use when validating limits associated to projects.
        Enforcement models will behave differently depending on the existing limits,
        which may result in backwards incompatible changes if a model is switched
        in a running deployment.
      max: null
      metavar: null
      min: null
      mutable: false
      name: enforcement_model
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: string value
    standard_opts:
    - driver
    - caching
    - cache_time
    - list_limit
    - enforcement_model
  wsgi:
    driver_option: ''
    driver_opts: {}
    dynamic_group_owner: ''
    help: ''
    opts:
    - advanced: false
      choices: []
      default: false
      deprecated_for_removal: false
      deprecated_opts: []
      deprecated_reason: null
      deprecated_since: null
      dest: debug_middleware
      help: 'If set to true, this enables the oslo debug middleware in Keystone. This
        Middleware prints a lot of information about the request and the response.
        It is useful for getting information about the data on the wire (decoded)
        and passed to the WSGI application pipeline. This middleware has no effect
        on the "debug" setting in the [DEFAULT] section of the config file or setting
        Keystone''s log-level to "DEBUG"; it is specific to debugging the WSGI data
        as it enters and leaves Keystone (specific request-related data). This option
        is used for introspection on the request and response data between the web
        server (apache, nginx, etc) and Keystone.  This middleware is inserted as
        the first element in the middleware chain and will show the data closest to
        the wire.  WARNING: NOT INTENDED FOR USE IN PRODUCTION. THIS MIDDLEWARE CAN
        AND WILL EMIT SENSITIVE/PRIVILEGED DATA.'
      max: null
      metavar: null
      min: null
      mutable: false
      name: debug_middleware
      namespace: keystone
      positional: false
      required: false
      sample_default: null
      secret: false
      short: null
      type: boolean value
    standard_opts:
    - debug_middleware

