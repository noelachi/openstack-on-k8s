#!/usr/bin/env bash

#@Todo. Add condition to generate db part only during install (if release.IsInstall)
#
#
echo "Database initialization ..."
MARIADB_HOST={{ .Values.mariadb.host | quote }}
MARIADB_ROOT_PASSWORD={{ .Values.mariadb.rootPassword | quote}}
PLACEMENT_DB_PASSWORD={{ .Values.authentication.password | quote}}
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "CREATE DATABASE IF NOT EXISTS placement;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON *.* TO 'placement'@'localhost' IDENTIFIED BY '${PLACEMENT_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON *.* TO 'placement'@'%' IDENTIFIED BY '${PLACEMENT_DB_PASSWORD}' WITH GRANT OPTION;"

echo "Testing the placement database"
mysql -u placement --password="${PLACEMENT_DB_PASSWORD}" --host="${MARIADB_HOST}" --database="placement" -e 'show databases;'
if [[ $? != 0 ]]; then
echo "Placement database test failed ..."
else
echo "Placement database test completed ..."
fi

placement-manage db sync
exit 0