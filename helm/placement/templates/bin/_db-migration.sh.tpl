#!/usr/bin/env bash

#@Todo. Add condition to generate db part only during install (if release.IsInstall)
#
#
echo "Database migration ..."

placement-manage db sync
placement-manage db online_data_migrations

exit 0