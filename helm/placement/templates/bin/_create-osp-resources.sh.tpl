
#!/usr/bin/env bash

for i in {1..30}
 do
 sleep 3
 curl http://keystone:5000/v3

 if [ $? -eq 0 ]; then
   source /scripts/adminrc.sh
   openstack user create --domain default --password {{ .Values.authentication.password }} placement
   openstack role add --project service --user placement admin
   openstack service create --name placement --description "Placement API" placement
   openstack endpoint create --region {{ .Values.authentication.regionName }} placement public http://{{ .Values.authentication.externalURL }}
   openstack endpoint create --region {{ .Values.authentication.regionName }} placement internal http://{{ include "placement.fullname" . }}:8778
   openstack endpoint create --region {{ .Values.authentication.regionName }} placement admin http://{{ include "placement.fullname" . }}:8778
   exit 0
 else
   echo $(date)" OpenStack API endpoint not available at this time."
 fi
 done
exit 1
