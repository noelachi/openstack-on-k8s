[DEFAULT]
use_stderr = True

[api]
auth_strategy = keystone

[cors]

[keystone_authtoken]
auth_url = http://{{ .Values.keystone.host }}:5000/v3
memcached_servers = {{ .Values.memcached.host }}:11211
auth_type = {{ .Values.authentication.type | default "password"}}
project_domain_name = {{ .Values.authentication.projectDomain | default "Default"}}
user_domain_name = {{ .Values.authentication.userDomain | default "Default"}}
project_name = {{ .Values.authentication.projectName | default "service"}}
username = {{ .Values.authentication.userName | default "placement"}}
password = {{ .Values.authentication.password }}

[oslo_policy]
policy_file = policy.yaml

[placement]


[placement_database]
connection = mysql+pymysql://{{ .Values.authentication.userName }}:{{ .Values.authentication.password }}@{{ .Values.mariadb.host }}/placement


[profiler]
