#!/usr/bin/env bash

#@Todo. Add condition to generate db part only during install (if release.IsInstall)
#
#

#
# Only post-upgrade
#
echo "Database migration ..."

nova-manage api_db sync
nova-manage db sync
nova-manage db sync --local_cell

exit 0