
#!/usr/bin/env bash

for i in {1..30}
 do
 sleep 3
 curl http://keystone:5000/v3

 if [ $? -eq 0 ]; then
   source /scripts/adminrc.sh
   openstack user create --domain default --password {{ .Values.authentication.novaPassword }} nova
   openstack role add --project service --user nova admin
   openstack service create --name nova --description "OpenStack Compute" compute
   openstack endpoint create --region {{ .Values.authentication.regionName }} compute public http://{{ .Values.authentication.externalURL }}/v2.1
   openstack endpoint create --region {{ .Values.authentication.regionName }} compute internal http://{{ include "glance.fullname" . }}:8774/v2.1
   openstack endpoint create --region {{ .Values.authentication.regionName }} compute admin http://{{ include "glance.fullname" . }}:8774/v2.1
   exit 0
 else
   echo $(date)" OpenStack API endpoint not available at this time."
 fi
 done
exit 1




