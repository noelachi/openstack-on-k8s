#!/usr/bin/env bash

#@Todo. Add condition to generate db part only during install (if release.IsInstall)
#
#
echo "Database initialization ..."
MARIADB_HOST={{ .Values.mariadb.host | quote }}
MARIADB_ROOT_PASSWORD={{ .Values.mariadb.rootPassword | quote}}
NOVA_DB_PASSWORD={{ .Values.authentication.dbPassword | quote}}
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "CREATE DATABASE IF NOT EXISTS nova_api;CREATE DATABASE IF NOT EXISTS nova;CREATE DATABASE IF NOT EXISTS nova_cell0;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' IDENTIFIED BY '${NOVA_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' IDENTIFIED BY '${NOVA_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' IDENTIFIED BY '${NOVA_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' IDENTIFIED BY '${NOVA_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' IDENTIFIED BY '${NOVA_DB_PASSWORD}' WITH GRANT OPTION;"
mysql -u root --password="${MARIADB_ROOT_PASSWORD}" --host="${MARIADB_HOST}" -e "GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' IDENTIFIED BY '${NOVA_DB_PASSWORD}' WITH GRANT OPTION;"
echo "Testing the Keystone database"
mysql -u nova --password="${NOVA_DB_PASSWORD}" --host="${MARIADB_HOST}" --database="nova" -e 'show databases;'
if [[ $? != 0 ]]; then
echo "Nova database test failed ..."
else
echo "Nova database test completed ..."
fi

su -s /bin/sh -c "nova-manage api_db sync" nova
su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
su -s /bin/sh -c "nova-manage db sync" nova

#nova-manage api_db sync
#nova-manage db sync
#nova-manage db online_data_migrations
#nova-manage db sync --local_cell
exit 0
