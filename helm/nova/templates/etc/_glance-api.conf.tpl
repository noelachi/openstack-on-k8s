[DEFAULT]
transport_url = rabbit://{{ .Values.rabbitmq.auth.username }}:{{ .Values.rabbitmq.auth.password }}@{{ .Values.rabbitmq.host }}:5672//
control_exchange = openstack

[cors]

[database]
connection = mysql+pymysql://{{ .Values.authentication.userName }}:{{ .Values.authentication.password }}@{{ .Values.mariadb.host }}/glance

[glance_store]
stores = rbd
default_store = rbd
# Path to the rootwrap configuration file to use for running commands as root.
#rootwrap_config = /etc/glance/rootwrap.conf
rbd_store_pool = images
rbd_store_user = glance
rbd_store_ceph_conf = /etc/ceph/ceph.conf
rbd_store_chunk_size = 8


[keystone_authtoken]
www_authenticate_uri  = http://{{ .Values.keystone.host }}:5000
auth_url = http://{{ .Values.keystone.host }}:5000
memcached_servers = {{ .Values.memcached.host }}:11211
auth_type = {{ .Values.authentication.type | default "password"}}
project_domain_name = {{ .Values.authentication.projectDomain | default "Default"}}
user_domain_name = {{ .Values.authentication.userDomain | default "Default"}}
project_name = {{ .Values.authentication.projectName | default "service"}}
username = {{ .Values.authentication.userName | default "glance"}}
password = {{ .Values.authentication.password }}
insecure = True

[paste_deploy]
flavor = keystone


