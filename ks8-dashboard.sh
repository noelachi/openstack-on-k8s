#!/bin/bash

kubectl -n kube-dashboard describe secret $(kubectl -n kube-dashboard get secret | grep admin-user | awk '{print $1}')
