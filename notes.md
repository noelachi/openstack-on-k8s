# Notes from Kubic
sudo chown -R apache: /var/www/html/
sudo chmod -R 755 /var/www/html

Create VMs

virsh vol-create-as --pool nvme --name cmp0-okd-120g1 120G --format qcow2  --allocation 0
virsh vol-create-as --pool nvme --name cmp0-okd-380g1 380G --format qcow2  --allocation 0
for i in {1..3}; do virsh vol-create-as --pool sshd --name cmp0-okd-360g$i 360G --format qcow2 --allocation 0; done
virt-install \
--name master0-ocp4 \
--memory 30720 \
--vcpus 10 \
--disk vol=nvme/cmp0-okd-120g1  \
--disk vol=nvme/cmp0-okd-380g1  \
--disk vol=sshd/cmp0-okd-360g1 \
--disk vol=sshd/cmp0-okd-360g2  \
--disk vol=sshd/cmp0-okd-360g3  \
--os-variant fedora32 \
--cdrom /var/lib/libvirt/images/clear-34000-live-server.iso \
--network network=br0,model=virtio,mac=52:54:00:e6:d5:a0 \
--network network=vmbr01,model=virtio \
--network network=vmbr02,model=virtio \
--cpu host-passthrough --hvm --vnc &

virsh vol-create-as --pool nvme --name cmp1-okd-120g1 120G --format qcow2  --allocation 0
virsh vol-create-as --pool nvme --name cmp1-okd-380g1 380G --format qcow2  --allocation 0
for i in {1..3}; do virsh vol-create-as --pool sshd --name cmp1-okd-360g$i 360G --format qcow2 --allocation 0; done
virt-install \
--name master1-ocp4 \
--memory 30720 \
--vcpus 10 \
--disk vol=nvme/cmp1-okd-120g1  \
--disk vol=nvme/cmp1-okd-380g1  \
--disk vol=sshd/cmp1-okd-360g1 \
--disk vol=sshd/cmp1-okd-360g2  \
--disk vol=sshd/cmp1-okd-360g3  \
--os-variant fedora32 \
--cdrom /var/lib/libvirt/images/clear-34000-live-server.iso \
--network network=br0,model=virtio,mac=52:54:00:20:e9:80 \
--network network=vmbr01,model=virtio \
--network network=vmbr02,model=virtio \
--cpu host-passthrough --hvm --vnc &

virsh vol-create-as --pool nvme --name cmp2-okd-120g1 120G --format qcow2  --allocation 0
virsh vol-create-as --pool nvme --name cmp2-okd-380g1 380G --format qcow2  --allocation 0
for i in {1..3}; do virsh vol-create-as --pool sshd --name cmp2-okd-360g$i 360G --format qcow2 --allocation 0; done
virt-install \
--name master2-ocp4 \
--memory 30720 \
--vcpus 10 \
--disk vol=nvme/cmp2-okd-120g1  \
--disk vol=nvme/cmp2-okd-380g1  \
--disk vol=sshd/cmp2-okd-360g1 \
--disk vol=sshd/cmp2-okd-360g2  \
--disk vol=sshd/cmp2-okd-360g3  \
--os-variant fedora32 \
--cdrom /var/lib/libvirt/images/clear-34000-live-server.iso \
--network network=br0,model=virtio,mac=52:54:00:20:e9:81 \
--network network=vmbr01,model=virtio \
--network network=vmbr02,model=virtio \
--cpu host-passthrough --hvm --vnc &

cat <<EOF | sudo tee /etc/sysconfig/network/ifcfg-enp1s0
BOOTPROTO='static'
STARTMODE='auto'
IPADDR='192.168.1.36/24'
MTU='0'
EOF

cat <<EOF | sudo tee /etc/sysconfig/network/ifcfg-eth1
BOOTPROTO='static'
STARTMODE='auto'
IPADDR='192.168.2.30/24'
MTU='0'
EOF

cat <<EOF | sudo tee /etc/sysconfig/network/ifcfg-eth2
BOOTPROTO='static'
STARTMODE='auto'
IPADDR='192.168.3.30/24'
MTU='0'
EOF

cat <<EOF | sudo tee /etc/sysconfig/network/ifroute-enp1s0
default 192.168.1.254 - enp1s0
EOF

sed -i 's/NETCONFIG_DNS_POLICY="auto"/NETCONFIG_DNS_POLICY=""/g' /etc/sysconfig/network/config /etc/sysconfig/network/config
sed -i 's/NETCONFIG_DNS_STATIC_SEARCHLIST=""/NETCONFIG_DNS_STATIC_SEARCHLIST="homelab.nac"/g' /etc/sysconfig/network/config /etc/sysconfig/network/config
sed -i 's/NETCONFIG_DNS_STATIC_SERVERS=""/NETCONFIG_DNS_STATIC_SERVERS="192.168.1.5"/g' /etc/sysconfig/network/config /etc/sysconfig/network/config

hostnamectl set-hostname compute-2.homelab.nac
```

# Deploy Kubernetes

## Deploy Kubernetes
https://linoxide.com/containers/install-kubernetes-on-ubuntu/
https://tech.osci.kr/2020/08/03/97477266/

1. Initialise master

```bash
sudo kubeadm init --apiserver-advertise-address=192.168.1.30 --pod-network-cidr=10.244.0.0/16
```

2. Configure network plugin

```bash
#calico
Edit network range before running kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml
https://docs.projectcalico.org/getting-started/kubernetes/quickstart

#canal
https://docs.projectcalico.org/getting-started/kubernetes/flannel/flannel
```

3. Join master and taint compute nodes

```bash
kubeadm join 192.168.1.30:6443 --token bokjiw.fdi18c21nafh5bu9 \
    --discovery-token-ca-cert-hash sha256:925a737d226d40e6fa122a5e63087ed8defdfe3c36a5a2721e8cb880a45f8dc6
```

4. Install CertManager 
https://cert-manager.io/docs/installation/kubernetes/
https://docs.cert-manager.io/en/release-0.8/tasks/issuers/setup-selfsigned.html

```bash
kubectl create namespace cert-manager
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.1.0/cert-manager.yaml

kube-0:~ # cat certmanager.yaml (cluster issuer does not work, using namespace issuer)
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigning-issuer
spec:
  selfSigned: {}

kubectl apply -f certmanager.yaml

using with ingresses https://cert-manager.io/docs/usage/ingress/
```

5. Metallb

The official dock works very well.
https://metallb.universe.tf/installation/


6. Install nginx ingress controller

The bitnami version works well with metallb while the one provided by nginx does not.

https://bitnami.com/stack/nginx-ingress-controller/helm

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
kubectl create namespace ingresses
helm install nginx-ing01 bitnami/nginx-ingress-controller -n ingresses
```

7. Install ceph
Have to define storage crush
To define storage pools, follow https://github.com/rook/rook/blob/master/Documentation/ceph-pool-crd.md
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-ceph-cluster-within-kubernetes-using-rook

```bash
cd $HOME/labs/rook/cluster/examples/kubernetes/ceph
kubectl create -f crds.yaml -f common.yaml -f operator.yaml
kubectl create -f cluster.yaml
kubectl apply -f dashboard-loadbalancer.yaml

#admin password
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath="{['data']['password']}" | base64 --decode && echo
|9d)mbJp:YXQLHr]*Es1
#change storage classes
kubectl apply -f toolbox.yaml
kubectl -n rook-ceph exec -it $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[0].metadata.name}') bash
ceph osd tree
ceph pg dump
ceph osd crush rm-device-class 5
ceph osd crush set-device-class nvme 5

#create storage pools
see provisioning/ceph/pools.yaml

#create storage classes
see provisioning/ceph/storageclasses.yaml

# set default storage class
kubectl get storageclass
kubectl patch storageclass hdd-standalone -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

9. Kubeapps

```bash
kubectl create namespace kubeapps
helm install kubeapps --namespace kubeapps bitnami/kubeapps --set ingress.enabled=true --set ingress.hostname=kubeapps.apps.k8s.homelab.nac
kubectl create serviceaccount kubeapps-operator
kubectl create clusterrolebinding kubeapps-operator --clusterrole=cluster-admin --serviceaccount=default:kubeapps-operator
kubectl apply -f https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.17.0/crds.yaml
kubectl apply -f https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.17.0/olm.yaml
kubectl get secret $(kubectl get serviceaccount kubeapps-operator -o jsonpath='{range .secrets[*]}{.name}{"\n"}{end}' | grep kubeapps-operator-token) -o jsonpath='{.data.token}' -o go-template='{{.data.token | base64decode}}' && echo
```

6. Taint and label nodes

```bash
kubectl taint node compute-0.homelab.nac role=compute:NoSchedule
kubectl taint node compute-1.homelab.nac role=compute:NoSchedule
kubectl taint node compute-2.homelab.nac role=compute:NoSchedule

#remove 
kubectl taint node compute-0.homelab.nac role=compute:NoSchedule-
kubectl taint node compute-1.homelab.nac role=compute:NoSchedule-
kubectl taint node compute-2.homelab.nac role=compute:NoSchedule-

kubectl label node  kube-cy4-kube002 openstack-control-plane=enabled

kubectl label node master-0 role=master
kubectl label node compute-0 role=compute
kubectl label node compute-1 role=compute
kubectl label node compute-2 role=compute

kubectl label node control-0.homelab.nac role=control
kubectl label node control-1.homelab.nac role=control
kubectl label node control-2.homelab.nac role=control

#to remove
kubectl taint node compute-0.homelab.nac node.kubernetes.io/compute:NoSchedule-
kubectl taint node compute-1.homelab.nac node.kubernetes.io/compute:NoSchedule-
kubectl taint node compute-2.homelab.nac node.kubernetes.io/compute:NoSchedule-
```

To schedule pods on that node, do

```bash
annotations:
  scheduler.alpha.kubernetes.io/tolerations: '[{"key":"species","value":"compute"}]'
```

7. Kubernetes Dashboard
https://artifacthub.io/packages/helm/k8s-dashboard/kubernetes-dashboard

```bash
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
kubectl create namespace kube-dashboard 
helm install kube-dashboard kubernetes-dashboard/kubernetes-dashboard  -n kube-dashboard
helm install kube-dashboard kubernetes-dashboard/kubernetes-dashboard --set service.type=LoadBalancer -n kube-dashboard --set service.loadBalancerIP=192
.168.1.63

helm install kube-dashboard kubernetes-dashboard/kubernetes-dashboard -n kube-dashboard 

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-dashboard
EOF

cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-dashboard
EOF

kubectl -n kube-dashboard describe secret $(kubectl -n kube-dashboard get secret | grep admin-user | awk '{print $1}')
```
8. Deploy Monitoring (posponed)
https://docs.bitnami.com/tutorials/integrate-logging-kubernetes-kibana-elasticsearch-fluentd/

# Deploy OpenStack

https://tech.osci.kr/2020/08/03/97477266/
https://kubernetes.github.io/ingress-nginx/user-guide/multiple-ingress/
https://docs.openstack.org/install-guide/environment-sql-database-rdo.html
https://opendev.org/openstack/openstack-helm
https://opendev.org/openstack/openstack-helm-infra

 1. Prepare
- Create openstack namespaces
- Create ingress controller that listen to openstack namespace only

2. Deploy Monitoring and logging (posponed)
https://docs.bitnami.com/tutorials/integrate-logging-kubernetes-kibana-elasticsearch-fluentd/
https://medium.com/@thulasya/deploy-elasticsearch-on-kubernetes-via-helm-in-google-kubernetes-cluster-da722f3a8883
https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes

2.1. Install elasticsearch
```bash
Elastic
```bash
helm install -n  logging elasticsearch elastic/elasticsearch --set nodeSelector."role"=compute --set volumeClaimTemplate.resources.requests.storage=220Gi --set rbac.create=true
```
2.2. Kibana
```
helm -n logging install kibana elastic/kibana --set service.type=LoadBalancer,nodeSelector."role"=master

```
2.3. Fluent-bit 
```bash
helm repo add fluent https://fluent.github.io/helm-charts
helm install -n  logging  fluent-bit fluent/fluent-bit -f values.yaml

kubectl apply -n logging -f  fluent-bit-service-account.yaml -f fluent-bit-role.yaml -f fluent-bit-role-binding.yaml
kubectl apply -n logging -f fluent-bit-configmap.yaml
kubectl apply -n logging -f fluent-bit-ds.yaml
```

`Last good doc`
https://docs.opstreelabs.in/logging-operator/

2.4. Kube-prometheus
kubectl create ns monitoring
helm upgrade -n monitoring --install prom bitnami/kube-prometheus --set global.storageClass=hdd-standalone,prometheus.stickySessions=true,prometheus.ingress.enabled=true,prometheus.ingress.hosts[0].name=prometheus.apps.kube.homelab.nac,prometheus.persistence.enabled=true,prometheus.persistence.size=100Gi,alertmanager.stickySessions=true,prometheus.retention=30d,alertmanager.ingress.enabled=true,alertmanager.ingress.hosts[0].name=alertmanager.apps.kube.homelab.nac,alertmanager.replicaCount=1,alertmanager.retention=30d,alertmanager.persistence.enabled=true,alertmanager.persistence.size=100Gi,prometheus.replicaCount=1

3. Install galera cluster 
https://docs.bitnami.com/kubernetes/infrastructure/mariadb-galera/

```bash
helm install -n  openstack galera bitnami/mariadb-galera --set fullnameOverride=galera --set rootUser.password=shroot --set galera.mariabackup.password=shroot --set nodeSelector."role"=compute --set persistence.storageClass=nvme-standalone --set persistence.size=80Gi 

#installation notes
** Please be patient while the chart is being deployed **

Tip:

  Watch the deployment status using the command:

    kubectl get sts -w --namespace default -l app.kubernetes.io/instance=galera

MariaDB can be accessed via port "3306" on the following DNS name from within your cluster:

    galera.default.svc.cluster.local

To obtain the password for the MariaDB admin user run the following command:

    echo "$(kubectl get secret --namespace default galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode)"

To connect to your database run the following command:

    kubectl run galera-client --rm --tty -i --restart='Never' --namespace default --image docker.io/bitnami/mariadb-galera:10.5.8-debian-10-r36 --command \
      -- mysql -h galera -P 3306 -uroot -p$(kubectl get secret --namespace default galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode) my_database

To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace default svc/galera 3306:3306 &
    mysql -h 127.0.0.1 -P 3306 -uroot -p$(kubectl get secret --namespace default galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode) my_database

To upgrade this helm chart:

    helm upgrade --namespace default galera bitnami/mariadb-galera \
      --set rootUser.password=$(kubectl get secret galera -o jsonpath="{.data.mariadb-root-password}" | base64 --decode) \
      --set db.name=my_database \
      --set galera.mariabackup.password=$(kubectl get secret --namespace default galera -o jsonpath="{.data.mariadb-galera-mariabackup-password}" | base64 --decode)
```

```bash
[luser@infra01 memcached]$ helm install -n  openstack galera bitnami/mysql --set fullnameOverride=galera,primary.persistence.storageClass=nvme-standalone,primary.persistence.size=80Gi,secondary.persistence.storageClass=nvme-standalone,secondary.persistence.size=80Gi
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/luser/ocp4/auth/kubeconfig
NAME: galera
LAST DEPLOYED: Wed Jan  6 22:44:30 2021
NAMESPACE: openstack
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

Tip:

  Watch the deployment status using the command: kubectl get pods -w --namespace openstack

Services:

  echo Primary: galera.openstack.svc.cluster.local:3306

Administrator credentials:

  echo Username: root
  echo Password : $(kubectl get secret --namespace openstack galera -o jsonpath="{.data.mysql-root-password}" | base64 --decode)

To connect to your database:

  1. Run a pod that you can use as a client:

      kubectl run galera-client --rm --tty -i --restart='Never' --image  docker.io/bitnami/mysql:8.0.22-debian-10-r44 --namespace openstack --command -- bash

  2. To connect to primary service (read/write):

      mysql -h galera.openstack.svc.cluster.local -uroot -pWqluA8DeMA my_database

To upgrade this helm chart:

  1. Obtain the password as described on the 'Administrator credentials' section and set the 'root.password' parameter as shown below:

      ROOT_PASSWORD=$(kubectl get secret --namespace openstack galera} -o jsonpath="{.data.mysql-root-password}" | base64 --decode)
      helm upgrade galera bitnami/mysql --set auth.rootPassword=$ROOT_PASSWORD
```


# LB IP addresses
192.168.1.61:443 kube-dashboard
192.168.1.62:8443 ceph-dashboard

4. Install rabbitmq
https://github.com/bitnami/charts/tree/master/bitnami/rabbitmq/#installing-the-chart

```bash
helm install -n  openstack rabbitmq bitnami/rabbitmq --set auth.username=openstack,auth.password=openstack1,auth.erlangCookie=secretcookie,nodeSelector."role"=compute, persistence.storageClass=nvme-standalone,persistence.size=20Gi,replicaCount=3,service.type=LoadBalancer

WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/luser/ocp4/auth/kubeconfig
NAME: rabbitmq
LAST DEPLOYED: Wed Jan  6 17:19:20 2021
NAMESPACE: openstack
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

Credentials:

    echo "Username      : openstack"
    echo "Password      : $(kubectl get secret --namespace openstack rabbitmq -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)"
    echo "ErLang Cookie : $(kubectl get secret --namespace openstack rabbitmq -o jsonpath="{.data.rabbitmq-erlang-cookie}" | base64 --decode)"

RabbitMQ can be accessed within the cluster on port  at rabbitmq.openstack.svc.

To access for outside the cluster, perform the following steps:

To Access the RabbitMQ AMQP port:

    echo "URL : amqp://127.0.0.1:5672/"
    kubectl port-forward --namespace openstack svc/rabbitmq 5672:5672

To Access the RabbitMQ Management interface:

    echo "URL : http://127.0.0.1:15672/"
    kubectl port-forward --namespace openstack svc/rabbitmq 15672:15672

2. Access RabbitMQ using using the obtained URL.

To Access the RabbitMQ Management interface:

1. Get the RabbitMQ Management URL and associate its hostname to your cluster external IP:

   export CLUSTER_IP=$(minikube ip) # On Minikube. Use: `kubectl cluster-info` on others K8s clusters
   echo "RabbitMQ Management: http://rabbitmq.apps.k8s.homelab.nac/"
   echo "$CLUSTER_IP  rabbitmq.apps.k8s.homelab.nac" | sudo tee -a /etc/hosts

2. Open a browser and access RabbitMQ Management using the obtained URL.
```

5. memcached

```bash
[luser@infra01 memcached]$ helm upgrade --install -n openstack memcached bitnami/memcached -f bitnami-values.yaml
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/luser/ocp4/auth/kubeconfig
Release "memcached" does not exist. Installing it now.
NAME: memcached
LAST DEPLOYED: Wed Jan  6 22:16:58 2021
NAMESPACE: openstack
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **
Memcached endpoints are exposed on the headless service named: memcached.
Please see https://github.com/memcached/memcached/wiki/ConfiguringClient to understand the Memcached model and need for client-based consistent hashing.
You might also want to consider more advanced routing/replication approaches with mcrouter: https://github.com/facebook/mcrouter/wiki/Replicated-pools-setup

To access the Memcached Prometheus metrics from outside the cluster execute the following commands:

    kubectl port-forward --namespace openstack svc/memcached-metrics 9150:9150 &
    curl http://127.0.0.1:9150/metrics
```

6. etcd (looks like not required in kubernetes)

7. Keystone
Doc: https://github.com/openstack/openstack-helm/tree/master/keystone/templates
 7.1. Install the container (see Dockerfile)
 7.2. Create config maps
 secret --> env variable --> config map
 7.3. Configure keystone service
 7.4. Add toolbox container to the keystone deployement (to run db sync, keystone manage, create openstack ressources, etc)
 7.4. Configure mariadb init job
 docker.io/bitnami/mariadb-galera:10.5.8-debian-10-r36

 Do not forget to set sessionafinity for services `sessionAffinity`



 Helm chart dry run
 helm install --generate-name --dry-run --debug mychart/charts/mysubchart